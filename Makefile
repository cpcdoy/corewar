CXX=g++
CXXFLAGS=-ggdb -Wall -Wextra -Werror -pedantic -std=c++14


OBJDIR=objects
PIT_DIR=pit
TRACK_DIR=track
PIT_OBJDIR=$(OBJDIR)/$(PIT_DIR)
TRACK_OBJDIR=$(OBJDIR)/$(TRACK_DIR)
VPATH=src
LIBS=-lboost_program_options

PIT_SRC=$(addprefix $(PIT_OBJDIR)/, \
		main.o parser.o output_stream.o state.o instructions.o utils.o\
)
TRACK_SRC=$(addprefix $(TRACK_OBJDIR)/, \
				main.o track.o ship.o decoder.o logger.o data.o init.o instructions.o\
				option_parser.o utils.o\
)


all: pit track


pit: $(PIT_OBJDIR) $(PIT_SRC)
	$(CXX) $(CXXFLAGS) $(PIT_SRC) -o $@

$(PIT_OBJDIR):
	@mkdir -p $(PIT_OBJDIR)

$(PIT_OBJDIR)/%.o: $(PIT_DIR)/%.cc
	$(CXX) $(CXXFLAGS) -c $< -o $@



track: $(TRACK_OBJDIR) $(TRACK_SRC)
	$(CXX) $(CXXFLAGS) $(TRACK_SRC) $(LIBS) -o $@

$(TRACK_OBJDIR):
	@mkdir -p $(TRACK_OBJDIR)

$(TRACK_OBJDIR)/%.o: $(TRACK_DIR)/%.cc
	$(CXX) $(CXXFLAGS) -c $< -o $@

parser: clean pit

clean:
	rm -rf $(OBJDIR) pit track

check:
	./tests/t-good.py

.PHONY: all, pit, track
