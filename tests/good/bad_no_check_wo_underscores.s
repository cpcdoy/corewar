.name "Ciscovery 2.0"
.comment "Oups, je me suis planté d'instruction."

    mode assegai
    ll r0, 384 - 10 - 128
    ll r10, 384 - 8 - 9 - 128
    ll r11, 384 - 8 - 9 - 6 - 128
    ll r12, qirexcode - str
    ll r13, 0x7fcf
    ll r2, qirexcode - postldb
    ll r4, two - jumptwo
    ll r1, 256 - 5

.align 64
    fork
    bnz r4

jumptwo:

.align 64
    fork
    ll r5, one - jumpone
    bz r5
jumpone:

finisher:
    ll r6, qirexcode - qirexjmp
    mode piranha
    b r6
qirexjmp:

#checker:
 #   ldb [], 0x00, 0x10
  #  check
   # b r10
    #stb [r0], 0x00, 0x10
    #ll r7, 10
    #addi r3, 1
    #cmp r7, r3
    #bs end - now
#now:
#    b r1
#end:
    # Write the check here


one:
    ll r0, - 10 - 256
    ll r10, - 8 - 9 - 256
    ll r11, - 8 - 9 - 6 - 256
    ll r3, preldb - two
    b r3
two:

preldb:
    ldb [r2], 0x0, 0x20
postldb:
    ll r8, 100
    check
    mode rocket
    ll r7, realcode - rocket
    b r7
rocket:

.align 63 nop

qirexcode:
    b r1
realcode:
    stb [r0], 0x0, 0x1f
    addi r15, 1
    cmp r15, r8
    bs r10
    str [r12], r13 # r13 is the bytecode
str:
    b r11
