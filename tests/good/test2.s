.name "Zork"
.comment "test"

mode icaras

ll r7, indirection-ind
ll r1, 7
ll r8, 50 #buffer offset
ll r6, plasmamode-plasmamodeoff
ll r0, 384-10-4+4
ll r2, 384-10-1+4
ll r4, 384-12-4+4
ll r3, 384-12-1+4


fork
bz r6
plasmamodeoff:
test:
.align 57
ldb [r1], 0, 62
stb [r2], 10, 10
b r0
stb [r2], 20, 10
b r0
stb [r2], 30, 10
b r0
stb [r2], 40, 10
b r0
stb [r3], 50, 12
b r4
check
stb [r2], 0, 10
b r0
ind:

plasmamode:
#nop
#nop
#nop
#nop
#nop
#nop
#nop
#nop
#nop
#nop
#nop
#nop
#nop
#nop
ldb [r1], 0, 4
nop
nop
check
ll r1, 0x91c7+188
ll r2, 0x91c7+188+16384-8
ll r3, 0x91c7+188+24576-16
ll r4, 0x91c7+188+32768-24+1
ll r5, 0x91c7+188+40960-32+1
ll r6, 0x91c7+188+49152-40+1
ll r7, 0x91c7+188+57344-48+1
ll r8, 0x91c7+188+65536-56

mode plasma
plasmaaction:
stb [r1], 0, 2
stb [r2], 0, 2
stb [r3], 0, 2
stb [r4], 0, 2
stb [r5], 0, 2
stb [r6], 0, 2
stb [r7], 0, 2
stb [r8], 2, 4

indirection:

