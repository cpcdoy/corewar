#!/usr/bin/env python

import random

modes_str = ['feisar', 'goteki45', 'agsystems', 'auricom', 'assegai', 'piranha',
        'qirex', 'icaras', 'rocket', 'missile', 'mine', 'plasma',
        'miniplasma'];

def to_bin(num):
    s = str(bin(num))
    n = '%'
    for i in range(2, len(s)):
        n += s[i]
    return n

def to_oct(num):
    s = str(oct(num))
    n = '0'
    for i in range(2, len(s)):
        n += s[i]
    return n

def to_hex(num):
    return str(hex(num))

def to_dec(num):
    return str(num)

def reg():
    return 'r' + str(random.choice(range(0, 16)))

def addr():
    return '[' + generate_reg() + ']'

def num():
    num = random.choice(range(0, int('ffff', 16) + 1))
    return random.choice([to_bin, to_oct, to_hex, to_dec])(num)

def orr(op):
    return op + ' ' + reg() + ', ' + reg()

def ori(op):
    return op + ' ' + reg() + ', ' + num()

# /* Called op 'cause or is a keyword */
def op(op):
    return op + ' ' + reg()

def add():
    return orr('add')

def addi():
    return ori('addi')

def and_():
    return orr('and')

def asr():
    return ori('asr')

def b():
    return op('b')

def bnz():
    return op('bz')

def bs():
    return op('bs')

def bz():
    return op('bz')

def check():
    return 'check'

def cmp():
    return orr('cmp')

def cmpi():
    return ori('cmpi')

def fork():
    return 'fork'

def lc():
    return ori('lc')

def ll():
    return ori('ll')

def mode():
    return 'mode ' + random.choice(modes_str)

def mov():
    return orr('mov')

def neg():
    return orr('neg')

def nop():
    return 'nop'

def not_():
    return orr('not')

def rol():
    return ori('rol')

def stat():
    return ori('stat')

def sub():
    return orr('sub')

def swp():
    return orr('swp')

def or_():
    return orr('or')

def write():
    return 'write ' + reg()

def xor():
    return orr('xor')

def inst():
    return random.choice([nop, lc, write, fork])()
    # return random.choice([add, addi, and_, asr,
                          # b, bnz, bs, bz,
                          # check, cmp, cmpi, lc, ll,
                          # mode, mov, neg, not_,
                          # rol, stat, sub, swp,
                          # or_, xor])()

for i in range(0, 10):
    print(inst())
