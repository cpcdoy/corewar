#! /usr/bin/env python3

from subprocess import DEVNULL
from subprocess import check_output
from os import walk
import os
import subprocess



blacklist = [".cor", ".swp", ".py", ".sh"]

def check_file(file):
    for name in blacklist:
        if name in file:
            return False
    return True

def test(files):
    for file in files:
        if check_file(file):
            try:
                output = check_output(
                            ("./pit", "-n", file),
                            stderr=DEVNULL
                        )
                print("\x1b[32m"" good""\x1b[0m " + file)
            except subprocess.CalledProcessError:
                print("\x1b[31m"" bad""\x1b[0m  " + file)


for (dirpath, dirnames, filenames) in walk("./tests/"):
    test([dirpath + '/' + filename for filename in filenames])
