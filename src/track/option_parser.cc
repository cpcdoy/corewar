#include "option_parser.hh"

namespace option_parser
{
  namespace po = boost::program_options;

  inline void Opt::option_mode(po::options_description& desc)
  {
    desc.add_options()(
        "mode,m",
        po::value<std::string>()->default_value("vector")->notifier(
            [](std::string value) {
              std::string values[] = {"vector", "venom", "rapier", "phantom"};
              for (auto val : values)
                if (val == value)
                  return;
              throw po::validation_error(
                  po::validation_error::invalid_option_value, "mode", value);
            }),
        "Sets the VM’s level: vector, venom, rapier, phantom. The default "
        "level is vector.");
  }

  inline void Opt::option_laps(po::options_description& desc)
  {
    desc.add_options()(
        "laps-number,l", po::value<int>()->notifier([](int value) {
          if (value < 1 || value > 65535)
            throw po::validation_error(
                po::validation_error::invalid_option_value, "laps-number",
                std::to_string(value));
        }),
        "Sets the number of laps required to win (default is LAPS_NUMBER). "
        "The maximum value is 65535, minimum is 1.");
  }

  inline void Opt::option_memory(po::options_description& desc)
  {
    desc.add_options()("memory-size,s", po::value<int>()->notifier([](
                                            int value) {
      if (value < 1 || value > 65536)
        throw po::validation_error(po::validation_error::invalid_option_value,
                                   "memory-size", std::to_string(value));
    }),
                       "Sets the size in nibbles of the memory "
                       "(default is MEMORY_SIZE nibbles)."
                       " The maximum value is 65536, minimum is 1.");
  }

  inline void Opt::option_check_laps(po::options_description& desc)
  {
    desc.add_options()(
        "checkpoints-per-lap,n", po::value<int>()->notifier([](int value) {
          if (value < 3 || value > 4096)
            throw po::validation_error(
                po::validation_error::invalid_option_value,
                "checkpoints-per-lap", std::to_string(value));
        }),
        "Sets the number of checkpoints per lap"
        " (default is CHECKPOINTS_PER_LAP checkpoints per lap).");
  }

  inline void Opt::option_check_delay(po::options_description& desc)
  {
    desc.add_options()(
        "checkpoint-delay,d", po::value<int>()->notifier([](int value) {
          if (value < 1 || value > 16777215)
            throw po::validation_error(
                po::validation_error::invalid_option_value, "checkpoint-delay",
                std::to_string(value));
        }),
        "Sets the maximum time (cycles) available to validate a checkpoint "
        "(default is CHECKPOINT_DELAY cycles). "
        "The maximum value is 16777215, minimum is 1.)");
  }

  inline void Opt::option_verbose(po::options_description& desc)
  {
    desc.add_options()("verbose,v", po::value<int>()->notifier([](int value) {
      if (value < 0 || value > 10)
        throw po::validation_error(po::validation_error::invalid_option_value,
                                   "verbose", std::to_string(value));
    }),
                       "Enables verbosity mode N. "
                       "See dedicated section for speci cs rules.");
  }

  void Opt::parse(int argc, char** argv)
  {
    try
    {
      po::options_description desc("Options");
      desc.add_options()("help,h", "Displays a help message")(
          "ship-file", po::value<std::string>(), "ship-file (.cor)");
      option_mode(desc);
      option_laps(desc);
      option_memory(desc);
      option_check_laps(desc);
      option_check_delay(desc);
      option_verbose(desc);

      po::variables_map vm;

      po::positional_options_description p;
      p.add("ship-file", -1);
      po::store(
          po::command_line_parser(argc, argv).options(desc).positional(p).run(),
          vm);

      if (vm.count("help"))
      {
        std::cout << desc << std::endl;
        super_exec_ = []() { return 0; };
        return;
      }
      vm.notify();
      file_ = vm["ship-file"].as<std::string>();
      if (vm.count("laps-number"))
        Data::data().set_laps_number(vm["laps-number"].as<int>());
      if (vm.count("memory_size"))
        Data::data().set_memory_size(vm["memory_size"].as<int>());
      if (vm.count("checkpoints-per-lap"))
        Data::data().set_memory_size(vm["checkpoints-per-lap"].as<int>());
      if (vm.count("checkpoint-delay"))
        Data::data().set_memory_size(vm["checkpoint-delay"].as<int>());
      if (vm.count("verbose"))
      {
        verbose_ = vm["verbose"].as<int>();
      }
    }
    catch (std::exception)
    {
      super_exec_ = std::bind(&Opt::error, this);
    }
  }

  Opt::Opt(int argc, char** argv)
  {
    Data::data().init();
    super_exec_ = std::bind(&Opt::exec, this);
    parse(argc, argv);
  }

  int Opt::error()
  {
    std::cout << "Damn, I wanted to kick ass, "
              << "and they feed me an error." << std::endl;
    return 2;
  }

  int Opt::exec()
  {
    Track track;
    std::vector<int> memory;
    if (verbose_ >= 3)
      std::cout << "No mode explicitly selected : using vector mode.\n";
    try
    {
      memory = load_file(file_);
    }
    catch (char const* msg) // TODO print error()
    {
      std::cerr << msg << "\n";
      return 1;
    }
    Logger::log().init(verbose_);
    track.load(memory);
    track.run();
    return 0;
  }

  int Opt::run()
  {
    return super_exec_();
  }
}
