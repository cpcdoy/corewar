#include "logger.hh"

#include <iostream>

Logger& Logger::log()
{
  static Logger instance;
  return instance;
}

void Logger::init(int verbose)
{
  if (verbose == 10)
    std::cout << "Verbose mode " << verbose << "\n";
  verbose_ = verbose;
  load_inst();
}

void Logger::load_inst()
{
  strs_[0] = &Logger::crash;
  strs_[1] = &Logger::nop;
  strs_[2] = &Logger::and_inst;
  strs_[3] = &Logger::or_inst;
  strs_[4] = &Logger::xor_inst;
  strs_[5] = &Logger::not_inst;
  strs_[6] = &Logger::rol_inst;
  strs_[7] = &Logger::asr_inst;
  strs_[8] = &Logger::add;
  strs_[9] = &Logger::sub;

  strs_[10] = &Logger::cmp;
  strs_[11] = &Logger::neg_inst;
  strs_[12] = &Logger::mov_inst;
  strs_[13] = &Logger::ldr;
  strs_[14] = &Logger::str;
  strs_[15] = &Logger::ldb;
  strs_[17] = &Logger::lc;
  strs_[18] = &Logger::ll;
  strs_[19] = &Logger::swp;
  strs_[20] = &Logger::addi;
  strs_[21] = &Logger::cmpi;
  strs_[22] = &Logger::b;
  strs_[23] = &Logger::bz;
  strs_[24] = &Logger::bnz;
  strs_[25] = &Logger::bs;
  strs_[26] = &Logger::stat;
  strs_[29] = &Logger::fork;
  strs_[30] = &Logger::write;
}

void Logger::inst(int id, int opcode, std::vector<int> nib)
{
  inst_.emplace(id, strs_[opcode](this, nib));
}

void Logger::print_inst(std::string inst)
{
  if (verbose_ == 10)
    std::cout << "    " << inst << "\n";
  else if (verbose_ == 3)
  {
    int val = 0;
    if (inst.size() > 15)
      val = inst.size() - 15;
    for (unsigned i = 0; i < 15 + val - inst.size(); ++i)
      std::cout << " ";
    std::cout << inst << " |";
  }
}

void Logger::print()
{
  if (!inst_.empty())
  {
    for (int id : ids_)
    {
      auto tmp = inst_.find(id);
      if (tmp != inst_.end())
        print_inst(tmp->second);
      else
        print_inst("");
    }
    if (verbose_ >= 3)
      std::cout << "\n";
    inst_.clear();
  }
  for (auto value : output_)
    std::cout << "0x" << std::hex << value << std::dec << "\n";
  output_.clear();
}

void Logger::notify(std::map<int, Ship>& ships)
{
  ids_.clear();
  for (auto t : ships)
    ids_.push_back(t.first);
}

void Logger::write_buf(short value)
{
  output_.push_back(value);
}

void Logger::cycle_count(int n)
{
  if (verbose_ == 10)
    std::cout << "cycle " << n << "\n";
}

void Logger::step(std::string step)
{
  if (verbose_ == 10)
    std::cout << "  " << step;
}

void Logger::store(int n)
{
  if (verbose_ == 10)
    std::cout << "    Stored " << n << "\n";
}

void Logger::pc_change(int pc)
{
  if (verbose_ == 10)
    std::cout << "    pc -> " << pc << "\n";
}

void Logger::time(int n)
{
  if (verbose_ == 10)
    std::cout << " -> " << n << "\n";
}

void Logger::log(std::string s)
{
  if (verbose_ == 10)
    std::cout << s << "\n";
}

/* Instructions */

std::string Logger::crash(std::vector<int> nibbles)
{
  nibbles.clear();
  return "crash";
}

std::string Logger::fork(std::vector<int> nibbles)
{
  nibbles.clear();
  return "fork";
}

std::string Logger::lc(std::vector<int> nibbles)
{
  int in = nibbles.at(2);
  int n = nibbles.at(3) + nibbles.at(4) * 16;
  return "lc r" + std::to_string(in) + ", " + utils::to_hex(n);
}

std::string Logger::nop(std::vector<int> nibbles)
{
  nibbles.clear();
  return "nop";
}

std::string Logger::write(std::vector<int> nibbles)
{
  return "write r" + std::to_string(nibbles.at(2));
}

std::string Logger::ldr(std::vector<int> nibbles)
{
  return "ldr r" + std::to_string(nibbles.at(1)) + ", [r" +
         std::to_string(nibbles.at(2)) + "]";
}

std::string Logger::str(std::vector<int> nibbles)
{
  return "str [r" + std::to_string(nibbles.at(1)) + "], r" +
         std::to_string(nibbles.at(2));
}

std::string Logger::ldb(std::vector<int> nibbles)
{
  int r = nibbles.at(3) + nibbles.at(4) * 16;
  return "ldb [r" + std::to_string(nibbles.at(2)) + "], " + std::to_string(r) +
         ", " + std::to_string(nibbles.at(5) + nibbles.at(6) * 16);
}

std::string Logger::ll(std::vector<int> nibbles)
{
  int in = nibbles.at(2);
  int n = nibbles.at(3) + nibbles.at(4) * 16 + nibbles.at(5) * pow(16, 2) +
          nibbles.at(6) * pow(16, 3);

  return "ll r" + std::to_string(in) + ", " + utils::to_hex(n);
}

std::string Logger::and_inst(std::vector<int> nibbles)
{
  return "and r" + std::to_string(nibbles.at(1)) + ", r" +
         std::to_string(nibbles.at(2));
}

std::string Logger::or_inst(std::vector<int> nibbles)
{
  return "or r" + std::to_string(nibbles.at(1)) + ", r" +
         std::to_string(nibbles.at(2));
}

std::string Logger::xor_inst(std::vector<int> nibbles)
{
  return "xor r" + std::to_string(nibbles.at(1)) + ", r" +
         std::to_string(nibbles.at(2));
}

std::string Logger::not_inst(std::vector<int> nibbles)
{
  return "not r" + std::to_string(nibbles.at(1)) + ", r" +
         std::to_string(nibbles.at(2));
}

std::string Logger::rol_inst(std::vector<int> nibbles)
{
  return "rol r" + std::to_string(nibbles.at(1)) + ", " +
         std::to_string(nibbles.at(2));
}

std::string Logger::asr_inst(std::vector<int> nibbles)
{
  return "asr r" + std::to_string(nibbles.at(1)) + ", " +
         std::to_string(nibbles.at(2));
}

std::string Logger::neg_inst(std::vector<int> nibbles)
{
  return "neg r" + std::to_string(nibbles.at(1)) + ", r" +
         std::to_string(nibbles.at(2));
}

std::string Logger::mov_inst(std::vector<int> nibbles)
{
  return "mov r" + std::to_string(nibbles.at(1)) + ", r" +
         std::to_string(nibbles.at(2));
}

std::string Logger::cmp(std::vector<int> nibbles)
{
  return "cmp r" + std::to_string(nibbles.at(1)) + ", r" +
         std::to_string(nibbles.at(2));
}

std::string Logger::add(std::vector<int> nibbles)
{
  return "add r" + std::to_string(nibbles.at(1)) + ", r" +
         std::to_string(nibbles.at(2));
}

std::string Logger::sub(std::vector<int> nibbles)
{
  return "sub r" + std::to_string(nibbles.at(1)) + ", r" +
         std::to_string(nibbles.at(2));
}

std::string Logger::swp(std::vector<int> nibbles)
{
  return "swp r" + std::to_string(nibbles.at(2)) + ", r" +
         std::to_string(nibbles.at(3));
}

std::string Logger::addi(std::vector<int> nibbles)
{
  return "addi r" + std::to_string(nibbles.at(2)) + ", " +
         std::to_string(nibbles.at(3));
}

std::string Logger::cmpi(std::vector<int> nibbles)
{
  return "cmpi r" + std::to_string(nibbles.at(2)) + ", " +
         std::to_string(nibbles.at(3));
}

std::string Logger::stat(std::vector<int> nibbles)
{
  return "stat r" + std::to_string(nibbles.at(2)) + ", " +
         std::to_string(nibbles.at(3));
}

std::string Logger::b(std::vector<int> nibbles)
{
  return "b r" + std::to_string(nibbles.at(2));
}

std::string Logger::bz(std::vector<int> nibbles)
{
  return "bz r" + std::to_string(nibbles.at(2));
}

std::string Logger::bs(std::vector<int> nibbles)
{
  return "bs r" + std::to_string(nibbles.at(2));
}

std::string Logger::bnz(std::vector<int> nibbles)
{
  return "bnz r" + std::to_string(nibbles.at(2));
}

int Logger::get_verbose()
{
  return verbose_;
}
