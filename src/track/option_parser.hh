#pragma once
#include <boost/program_options.hpp>
#include <functional>
#include <iostream>
#include <string>

#include "data.hh"
#include "init.hh"
#include "logger.hh"
#include "ship.hh"
#include "track.hh"

namespace option_parser
{
  namespace po = boost::program_options;

  class Opt
  {
    typedef int (*Handler)();

  private:
    std::string file_;
    int verbose_ = 0;
    std::function<int(void)> super_exec_;

  public:
    Opt(int argc, char** argv);
    int run();

  private:
    void option_mode(po::options_description& desc);
    void option_laps(po::options_description& desc);
    void option_memory(po::options_description& desc);
    void option_check_laps(po::options_description& desc);
    void option_check_delay(po::options_description& desc);
    void option_verbose(po::options_description& desc);
    void parse(int argc, char** argv);
    int error();
    int exec();
  };
}
