#include <iostream>
#include <sstream>
#include <string>

namespace utils
{
  std::string to_hex(int n);
  std::string to_hex(short n);
}
