#include "decoder.hh"
#include "logger.hh"
#include "ship.hh"

#include <iostream>

Decoder::Decoder()
{
  inst_[0] = &Ship::crash;
  inst_[1] = &Ship::nop;
  inst_[2] = &Ship::and_inst;
  inst_[3] = &Ship::or_inst;
  inst_[4] = &Ship::xor_inst;
  inst_[5] = &Ship::not_inst;
  inst_[6] = &Ship::rol_inst;
  inst_[7] = &Ship::asr_inst;
  inst_[8] = &Ship::add;
  inst_[9] = &Ship::sub;
  inst_[10] = &Ship::cmp;
  inst_[11] = &Ship::neg_inst;
  inst_[12] = &Ship::mov_inst;
  inst_[13] = &Ship::ldr;
  inst_[14] = &Ship::str;
  inst_[15] = &Ship::ldb;
  inst_[17] = &Ship::lc;
  inst_[18] = &Ship::ll;
  inst_[19] = &Ship::swp;
  inst_[20] = &Ship::addi;
  inst_[21] = &Ship::cmpi;
  inst_[22] = &Ship::b;
  inst_[23] = &Ship::bz;
  inst_[24] = &Ship::bnz;
  inst_[25] = &Ship::bs;
  inst_[26] = &Ship::stat;
  inst_[29] = &Ship::fork;
  inst_[30] = &Ship::write;

  sizes_[0] = 1;
  sizes_[1] = 1;
  sizes_[2] = 3;
  sizes_[3] = 3;
  sizes_[4] = 3;
  sizes_[5] = 3;
  sizes_[6] = 3;
  sizes_[7] = 3;
  sizes_[8] = 3;
  sizes_[9] = 3;
  sizes_[10] = 3;
  sizes_[11] = 3;
  sizes_[12] = 3;
  sizes_[13] = 3;
  sizes_[14] = 3;
  sizes_[15] = 7;
  sizes_[17] = 5;
  sizes_[18] = 7;
  sizes_[19] = 4;
  sizes_[20] = 4;
  sizes_[21] = 4;
  sizes_[22] = 3;
  sizes_[23] = 3;
  sizes_[24] = 3;
  sizes_[25] = 3;
  sizes_[26] = 4;
  sizes_[29] = 2;
  sizes_[30] = 3;
}

void Decoder::clear()
{
  nibbles_.clear();
}

void Decoder::store(int nib)
{
  Logger::log().store(nib);
  nibbles_.push_back(nib);
}

int Decoder::decode()
{
  unsigned int size = nibbles_.size();
  int first = nibbles_.at(0);
  int opcode = first;

  if (first == 15)
  {
    if (size < 2)
      return -1;
    opcode = first + nibbles_.at(1);
  }

  if (size < sizes_[opcode])
    return -1;

  return opcode;
}

std::vector<int> Decoder::get_nibbles()
{
  return nibbles_;
}

std::function<void(Ship*, std::vector<int>)> Decoder::get_inst(int opcode)
{
  return inst_[opcode];
}

int Decoder::get_size(int opcode)
{
  return sizes_[opcode];
}
