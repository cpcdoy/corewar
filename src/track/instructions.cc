#include "instructions.hh"

namespace utils
{
  unsigned int inst_size_imm(enum inst_size s)
  {
    return static_cast<unsigned int>(s);
  }

  unsigned int inst_size(uint8_t high, uint8_t low)
  {
    static std::map<uint16_t, unsigned int> op;

    if (op.empty())
    {
      for (int i = 0; i < 2; i++)
        op.insert(std::make_pair(i, 1));
      for (int i = 2; i < 13; i++)
        op.insert(std::make_pair(i, 3));
      //
      for (int i = 0; i < 2; i++)
        op.insert(std::make_pair((opcodes::ext_ << 4) | i, 7));
      op.insert(std::make_pair((opcodes::ext_ << 4) | 2, 5));
      op.insert(std::make_pair((opcodes::ext_ << 4) | 3, 7));
      for (int i = 4; i < 7; i++)
        op.insert(std::make_pair((opcodes::ext_ << 4) | i, 4));
      for (int i = 7; i < 4 + 7; i++)
        op.insert(std::make_pair((opcodes::ext_ << 4) | i, 3));
      op.insert(std::make_pair((opcodes::ext_ << 4) | 11, 4));
      op.insert(std::make_pair((opcodes::ext_ << 4) | 12, 2));
      op.insert(std::make_pair((opcodes::ext_ << 4) | 13, 3));
      op.insert(std::make_pair((opcodes::ext_ << 4) | 14, 2));
      op.insert(std::make_pair((opcodes::ext_ << 4) | 15, 3));
    }

    return op[(high << 4) | low];
  }
}
