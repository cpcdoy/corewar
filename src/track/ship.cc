#include "ship.hh"
#include "data.hh"
#include "logger.hh"
#include "track.hh"

#include <assert.h>
#include <cmath>
#include <iostream>
#include <sstream>

Ship::Ship(Track* track, int id) : regs_(), decoder_(), id_(id), buffer_()
{
  pipidx_ = 0;
  track_ = track;
  distance = 0;
  wait_counter_ = 0;
  opcode_ = 0;
  mode_ = 0;
  pc = 0;
  checks_ = 1;
  check_timer_ = 0;
  (void)buffer_;
}

void Ship::init()
{
  pipeline_[0] = &Ship::fetch;
  pipeline_[1] = &Ship::decode;
  pipeline_[2] = &Ship::load_inst;
  pipeline_[3] = &Ship::execute;
}

void Ship::make_forked(int id)
{
  id_ = id;
  regs_[16] = 1;
}

void Ship::update()
{
  if (wait_counter_ > 0)
    wait_counter_--;
  else
    while (pipidx_ < pipeline_.size())
    {
      /* std::cout << "pipidx_ -> " << pipidx_ << "\n"; */
      if (pipeline_[pipidx_](*this))
        break;
      pipidx_++;
    }
  pipidx_ %= pipeline_.size(); /* Back to fetch when over */
}

/* Pipeline */

int Ship::fetch()
{
  Logger::log().step("fetch()\n");
  int n = pc % Data::data().get_memory_size(); /* FIXME: modulo by the view */
  int nibble = track_->fetch(n);
  Logger::log().log("    fetched " + std::to_string(nibble) + " at address " +
                    std::to_string(n));
  decoder_.store(nibble);
  advance(1);
  return 0;
}

int Ship::decode()
{
  Logger::log().step("decode()");
  opcode_ = decoder_.decode();
  if (opcode_ == -1)
    return !(pipidx_ = 0); // break

  wait_counter_ = Data::data().get_delay_decode(opcode_, mode_);
  Logger::log().time(wait_counter_);
  Logger::log().inst(id_, opcode_, decoder_.get_nibbles());
  if (wait_counter_--)
    return pipidx_++; /* break and pass to load_inst */
  return 0;
}

int Ship::load_inst()
{
  Logger::log().step("load_inst()");
  wait_counter_ = Data::data().get_delay_execute(opcode_, mode_);
  Logger::log().time(wait_counter_);
  if (wait_counter_--)
    return pipidx_++;
  return 0;
}

int Ship::execute()
{
  Logger::log().step("execute(" + std::to_string(opcode_) + ")\n");
  decoder_.get_inst(opcode_)(this, decoder_.get_nibbles());
  return executing_;
}

/* Helpers */

void Ship::advance(int n)
{
  pc += n % MEMORY_SIZE;
  distance += n;
  Logger::log().pc_change(pc);
}

bool Ship::validate_check(int cycles)
{
  return CHECKPOINT_DELAY > cycles - check_timer_;
}

/* Instructions */

void Ship::b(std::vector<int> nibbles)
{
  decoder_.clear();
  int dist = regs_[nibbles.at(2)];
  if (regs_[nibbles.at(2)] > 0)
    while (dist >= Data::data().get_idx_mod(mode_))
    {
      dist -= 2 * Data::data().get_idx_mod(mode_);
    }
  else
    while (dist < -Data::data().get_idx_mod(mode_))
    {
      dist += 2 * Data::data().get_idx_mod(mode_);
    }
  pc += dist;

  if (pc < 0)
    pc += Data::data().get_memory_size();
}

void Ship::bz(std::vector<int> nibbles)
{
  decoder_.clear();
  if (regs_[16] == 1)
    b(nibbles);
}

void Ship::bs(std::vector<int> nibbles)
{
  decoder_.clear();
  if (regs_[17] == 1)
    b(nibbles);
}

void Ship::bnz(std::vector<int> nibbles)
{
  if (regs_[16] == 0)
    b(nibbles);
}

void Ship::or_inst(std::vector<int> nibbles)
{
  decoder_.clear();
  int in = nibbles.at(1);
  int out = nibbles.at(2);
  regs_[in] = regs_[in] | regs_[out];
  set_ZS(regs_[in]);
}

void Ship::xor_inst(std::vector<int> nibbles)
{
  decoder_.clear();
  int in = nibbles.at(1);
  int out = nibbles.at(2);
  regs_[in] = regs_[in] ^ regs_[out];
  set_ZS(regs_[in]);
}

void Ship::not_inst(std::vector<int> nibbles)
{
  decoder_.clear();
  int in = nibbles.at(1);
  int out = nibbles.at(2);
  regs_[in] = ~regs_[out];
  set_ZS(regs_[in]);
}

void Ship::rol_inst(std::vector<int> nibbles)
{
  decoder_.clear();
  int in = nibbles.at(1);
  int n = nibbles.at(2);
  regs_[in] = (regs_[in] << n) | (regs_[in] >> (16 - n));
  set_ZS(regs_[in]);
}

void Ship::asr_inst(std::vector<int> nibbles)
{
  decoder_.clear();
  int in = nibbles.at(1);
  int n = nibbles.at(2);
  regs_[in] >>= n;
  set_ZS(regs_[in]);
}

void Ship::neg_inst(std::vector<int> nibbles)
{
  decoder_.clear();
  int in = nibbles.at(1);
  int out = nibbles.at(2);
  regs_[in] = -regs_[out];
  set_ZS(regs_[in]);
}

void Ship::mov_inst(std::vector<int> nibbles)
{
  decoder_.clear();
  int in = nibbles.at(1);
  int out = nibbles.at(2);
  regs_[in] = regs_[out];
}

void Ship::add(std::vector<int> nibbles)
{
  decoder_.clear();
  int in = nibbles.at(1);
  int out = nibbles.at(2);
  regs_[in] = regs_[in] + regs_[out];
  set_ZS(regs_[in]);
}

void Ship::sub(std::vector<int> nibbles)
{
  decoder_.clear();
  int in = nibbles.at(1);
  int out = nibbles.at(2);
  regs_[in] = regs_[in] - regs_[out];
  set_ZS(regs_[in]);
}

void Ship::swp(std::vector<int> nibbles)
{
  decoder_.clear();
  int in = nibbles.at(2);
  int n = nibbles.at(3);
  auto tmp = regs_[in];
  regs_[in] = regs_[n];
  regs_[n] = tmp;
  set_ZS(regs_[in]);
}

void Ship::check(std::vector<int> nibbles)
{
  nibbles.clear();
  decoder_.clear();
  int zone_size = MEMORY_SIZE / CHECKPOINTS_PER_LAP;
  /* Increment if in zone */
  checks_ += (distance > zone_size && distance <= 2 * zone_size);
  check_timer_ = track_->get_cycles();
}

void Ship::crash(std::vector<int> nibbles)
{
  nibbles.clear();
  decoder_.clear();
  track_->crash(id_);
}

void Ship::fork(std::vector<int> nibbles)
{
  nibbles.clear();
  decoder_.clear();
  track_->fork(id_);
}

void Ship::lc(std::vector<int> nibbles)
{
  assert(nibbles.size() > 4);
  int in = nibbles.at(2);
  unsigned int n = (nibbles.at(3) & 0xf) | (nibbles.at(4) << 4);
  n &= 0xff;
  unsigned int sign = n > 1 << 7 ? -1 : 0;
  struct byte by = {(unsigned int)n, sign};
  union lc_byte b = {by};
  std::stringstream value;
  value << std::hex << b.full_byte;
  decoder_.clear();

  regs_[in] = b.full_byte;
}

void Ship::ll(std::vector<int> nibbles)
{
  decoder_.clear();
  int in = nibbles.at(2);
  regs_[in] = nibbles.at(3) + nibbles.at(4) * 16 + nibbles.at(5) * pow(16, 2) +
              nibbles.at(6) * pow(16, 3);
}

void Ship::ldr(std::vector<int> nibbles)
{
  static int count = 0;
  static int n = 0;
  static int in = 0;
  static int out = 0;
  static int nb = 0;
  decoder_.clear();

  if (count < 3)
  {
    track_->dec_cycles();
    if (count == 0)
    {
      in = nibbles.at(1);
      out = nibbles.at(2);
      int val = regs_[out];
      if (val > 0)
        while (val >= Data::data().get_idx_mod(mode_))
        {
          val -= 2 * Data::data().get_idx_mod(mode_);
        }
      else
        while (val < -Data::data().get_idx_mod(mode_))
        {
          val += 2 * Data::data().get_idx_mod(mode_);
        }
      n = pc + val;

      if (n < 0)
        n += Data::data().get_memory_size();

      n %= Data::data().get_memory_size();
    }
    int mem = track_->fetch(n + 3 - count);
    nb |= (mem & 0xf) << ((3 - count) * 4);
    track_->fetch(n + count);
    executing_ = 1;
    pipidx_ = 2;
    count++;
  }
  else
  {
    int mem = track_->fetch(n);
    nb |= (mem & 0xf);
    regs_[in] = nb;

    n = 0;
    in = 0;
    out = 0;
    executing_ = 0;
    count = 0;
  }
}

void Ship::store_buffer(int addr, int nib)
{
  buffer_[addr % 64] = nib;
}

void Ship::ldb(std::vector<int> nibbles)
{
  static int count = 0;
  static int n = 0;
  static int in = 0;
  static int out = 0;
  static int count_max = 2;
  decoder_.clear();

  if (count < count_max - 1)
  {
    track_->dec_cycles();
    if (count == 0)
    {
      count_max = nibbles.at(5) + nibbles.at(6) * 16;

      in = nibbles.at(3) + nibbles.at(4) * 16;
      out = nibbles.at(2);
      int val = regs_[out];
      if (val > 0)
        while (val >= Data::data().get_idx_mod(mode_))
        {
          val -= 2 * Data::data().get_idx_mod(mode_);
        }
      else
        while (val < -Data::data().get_idx_mod(mode_))
        {
          val += 2 * Data::data().get_idx_mod(mode_);
        }
      n = pc + val;

      if (n < 0)
        n += Data::data().get_memory_size();

      n %= Data::data().get_memory_size();
    }
    int mem = track_->fetch(n + count_max - count - 1);
    store_buffer(in + count_max - count - 1, mem);
    executing_ = 1;
    pipidx_ = 2;
    count++;
  }
  else
  {
    int mem = track_->fetch(n);
    store_buffer(in, mem);

    n = 0;
    in = 0;
    out = 0;
    executing_ = 0;
    count = 0;
  }
}

void Ship::str(std::vector<int> nibbles)
{
  static int count = 0;
  static int n = 0;
  static int in = 0;
  static int out = 0;
  static int nb[4];
  int masks[4] = {0xf, 0x00f0, 0x0f00, 0xf000};
  decoder_.clear();

  if (count < 3)
  {
    track_->dec_cycles();
    if (count == 0)
    {
      in = nibbles.at(1);
      (void)in;
      out = nibbles.at(2);
      int val = regs_[in];
      if (val > 0)
        while (val >= Data::data().get_idx_mod(mode_))
        {
          val -= 2 * Data::data().get_idx_mod(mode_);
        }
      else
        while (val < -Data::data().get_idx_mod(mode_))
        {
          val += 2 * Data::data().get_idx_mod(mode_);
        }
      n = pc + val;

      n %= Data::data().get_memory_size();

      if (n < 0)
        n += Data::data().get_memory_size();
    }
    nb[3 - count] = (regs_[out] & masks[3 - count]) >> ((3 - count) * 4);
    track_->store_in_mem(n + 3 - count, nb[3 - count]);
    executing_ = 1;
    pipidx_ = 2;
    count++;
  }
  else
  {
    nb[0] = (regs_[out] & 0xf);
    track_->store_in_mem(n, nb[0]);

    n = 0;
    in = 0;
    out = 0;
    executing_ = 0;
    count = 0;
  }
}

void Ship::cmp(std::vector<int> nibbles)
{
  decoder_.clear();
  int left = nibbles.at(1);
  int right = nibbles.at(2);
  int res = regs_[left] - regs_[right];
  set_ZS(res);
}

void Ship::cmpi(std::vector<int> nibbles)
{
  decoder_.clear();
  int in = nibbles.at(2);
  int n = nibbles.at(3);
  if (n >= (1 << 3))
    n = -1 - (0xf - n);
  int res = regs_[in] - n;
  set_ZS(res);
}

void Ship::addi(std::vector<int> nibbles)
{
  decoder_.clear();
  int in = nibbles.at(2);
  int n = nibbles.at(3);
  if (n >= (1 << 3))
    n = -1 - (0xf - n);
  regs_[in] = regs_[in] + n;
  set_ZS(regs_[in]);
}

void Ship::set_ZS(int value)
{
  regs_[16] = value ? 0 : 1;
  regs_[17] = value >> 15 ? 2 : 0;
}

void Ship::and_inst(std::vector<int> nibbles)
{
  decoder_.clear();
  int left = nibbles.at(1);
  int right = nibbles.at(2);
  regs_[left] = regs_[left] & regs_[right];
  set_ZS(regs_[left]);
}

void Ship::nop(std::vector<int> nibbles)
{
  nibbles.clear();
  decoder_.clear();
}

void Ship::stat(std::vector<int> nibbles)
{
  decoder_.clear();
  int in = nibbles.at(2);
  int n = nibbles.at(3);

  /* stat 2 and 3 not implemented */
  /* BEURK */
  if (n == 0)
    regs_[in] = mode_;
  else if (n == 1)
    regs_[in] = pc;
  else if (n == 2)
    assert(false);
  else if (n == 3)
    assert(false);
  else if (n == 4)
    regs_[in] = checks_;
  else if (n == 5)
    assert(false);
  else if (n == 6)
    regs_[in] = Data::data().get_memory_size();
  else if (n == 7)
    assert(false);
  else if (n == 8)
    regs_[in] = Data::data().get_check_per_laps();
  else if (n == 9)
    assert(false);
  else if (n == 10)
    assert(false);
  else if (n == 11)
    assert(false);
  else if (n == 12)
    assert(false);
  else if (n == 13)
    assert(false);
  else if (n == 14)
    assert(false);
  else if (n == 15)
    regs_[in] = regs_[16] + regs_[17];
}

void Ship::write(std::vector<int> nibbles)
{
  int out = nibbles.at(2);
  Logger::log().write_buf((short)regs_[out]);
  decoder_.clear();
}
