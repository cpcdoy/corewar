#ifndef TRACK_HH
#define TRACK_HH
#include "change.hh"

#include <array>
#include <map>
#include <vector>

class Ship;

class Track
{
public:
  Track();
  void fork_ships();
  void fork(int id);
  void crash(int id_);
  void crash_ships();
  void destroy();
  void update();
  void write();
  void fork();
  void check();
  int fetch(int n);
  void load(std::vector<int> bytecode);
  void run();
  void write(int n, int nibble);

  int get_cycles();
  void dec_cycles();

  void store_in_mem(int addr, int nib);

private:
  std::vector<int> mem_;
  std::map<int, Ship> ships_;
  std::vector<int> crashes_;
  std::vector<int> forks_;
  int cycles_;
  int id_;
};

#endif
