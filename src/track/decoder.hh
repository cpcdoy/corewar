#ifndef DECODER_HH
#define DECODER_HH

#include <array>
#include <functional>
#include <vector>

class Ship;

class Decoder
{
public:
  Decoder();
  int decode();
  void store(int nibble);
  void clear();
  std::vector<int> get_nibbles();
  std::function<void(Ship*, std::vector<int>)> get_inst(int opcode);
  int get_size(int opcode);

private:
  std::vector<int> nibbles_;
  std::function<void()> decode_;
  std::array<std::function<void(Ship*, std::vector<int>)>, 31> inst_;
  std::array<unsigned int, 31> sizes_;
};

#endif
