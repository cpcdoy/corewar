#include "utils.hh"

namespace utils
{
  std::string to_hex(int n)
  {
    std::stringstream value;
    value << std::hex << n;
    return value.str();
  }

  std::string to_hex(short n)
  {
    std::stringstream value;
    value << std::hex << n;
    return value.str();
  }
}
