#ifndef LOGGER_HH
#define LOGGER_HH
#include "ship.hh"
#include <array>
#include <functional>
#include <map>
#include <string>
#include <tuple>
#include <vector>

class Logger
{
public:
  static Logger& log();
  void init(int verbose);
  void load_inst();

  void inst(int id, int opcode, std::vector<int> nib);
  void print();
  void print_inst(std::string inst);
  void cycle_count(int n);
  void step(std::string step);
  void pc_change(int pc);
  void notify(std::map<int, Ship>& ids);
  void store(int n);
  void time(int n);
  void log(std::string s);
  void write_buf(short value);

  int get_verbose();

  /* Logging for each inst */

  std::string crash(std::vector<int> nibbles);
  std::string fork(std::vector<int> nibbles);
  std::string lc(std::vector<int> nibbles);
  std::string nop(std::vector<int> nibbles);
  std::string write(std::vector<int> nibbles);

  std::string ll(std::vector<int> nibbles);
  std::string ldr(std::vector<int> nibbles);
  std::string ldb(std::vector<int> nibbles);
  std::string str(std::vector<int> nibbles);
  std::string add(std::vector<int> nibbles);
  std::string sub(std::vector<int> nibbles);
  std::string addi(std::vector<int> nibbles);
  std::string cmp(std::vector<int> nibbles);
  std::string cmpi(std::vector<int> nibbles);
  std::string and_inst(std::vector<int> nibbles);
  std::string or_inst(std::vector<int> nibbles);
  std::string xor_inst(std::vector<int> nibbles);
  std::string not_inst(std::vector<int> nibbles);
  std::string rol_inst(std::vector<int> nibbles);
  std::string asr_inst(std::vector<int> nibbles);
  std::string neg_inst(std::vector<int> nibbles);
  std::string mov_inst(std::vector<int> nibbles);
  std::string swp(std::vector<int> nibbles);
  std::string stat(std::vector<int> nibbles);

  std::string b(std::vector<int> nibbles);
  std::string bz(std::vector<int> nibbles);
  std::string bnz(std::vector<int> nibbles);
  std::string bs(std::vector<int> nibbles);

private:
  int verbose_;
  std::vector<int> ids_; /* Ids of ships running */
  std::vector<short> output_;
  std::map<int, std::string> inst_;
  std::array<std::function<std::string(Logger*, std::vector<int>)>, 31> strs_;
};
#endif
