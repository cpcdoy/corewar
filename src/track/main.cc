#include <iostream>

#include "option_parser.hh"

int main(int argc, char** argv)
{
  return option_parser::Opt(argc, argv).run();
}
