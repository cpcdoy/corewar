#ifndef DATA_HH
#define DATA_HH

#include <array>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

class Data
{
private:
  int laps_number_;
  int memory_size_;
  int check_per_laps_;
  int check_delay_;
  int blue_arrow_spacing_;
  std::array<int, 819> code_;
  std::array<std::string, 31> names_;

public:
  static Data& data();
  void init();

public:
  int get_delay_decode(int opcode, int mode);
  int get_delay_execute(int opcode, int mode);
  std::string get_inst_name(int opcode);
  int get_laps_number();
  int get_memory_size();
  int get_check_per_laps();
  int get_check_delay();
  int get_blue_arrow_spacing();
  int get_idx_mod(int mode);

public:
  void set_laps_number(int n);
  void set_memory_size(int n);
  void set_check_per_laps(int n);
  void set_check_delay(int n);
  void set_blue_arrow_spacing(int n);

private:
  void load_constants();
  void load_names();
  void load_decode_crash();
  void load_decode_nop();
  void load_decode_and();
  void load_decode_or();
  void load_decode_xor();
  void load_decode_not();
  void load_decode_rol();
  void load_decode_asr();
  void load_decode_add();
  void load_decode_sub();
  void load_decode_cmp();
  void load_decode_neg();
  void load_decode_mov();
  void load_decode_ldr();
  void load_decode_str();
  void load_decode_ldb();
  void load_decode_stb();
  void load_decode_lc();
  void load_decode_ll();
  void load_decode_swp();
  void load_decode_addi();
  void load_decode_cmpi();
  void load_decode_b();
  void load_decode_bz();
  void load_decode_bnz();
  void load_decode_bs();
  void load_decode_stat();
  void load_decode_check();
  void load_decode_mode();
  void load_decode_fork();
  void load_decode_write();
  void load_execute_crash();
  void load_execute_nop();
  void load_execute_and();
  void load_execute_or();
  void load_execute_xor();
  void load_execute_not();
  void load_execute_rol();
  void load_execute_asr();
  void load_execute_add();
  void load_execute_sub();
  void load_execute_cmp();
  void load_execute_neg();
  void load_execute_mov();
  void load_execute_ldr();
  void load_execute_str();
  void load_execute_ldb();
  void load_execute_stb();
  void load_execute_lc();
  void load_execute_ll();
  void load_execute_swp();
  void load_execute_addi();
  void load_execute_cmpi();
  void load_execute_b();
  void load_execute_bz();
  void load_execute_bnz();
  void load_execute_bs();
  void load_execute_stat();
  void load_execute_check();
  void load_execute_mode();
  void load_execute_fork();
  void load_execute_write();
  void load_idx_mods();
};
#endif
