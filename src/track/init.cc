#include <fstream>
#include <iostream>

#include "init.hh"

std::vector<int> load_file(std::string file)
{
  std::ifstream f(file, std::ios::binary);

  /* Find the file's position */

  std::streampos begin = f.tellg();
  f.seekg(0, std::ios::end);
  std::streampos end = f.tellg();
  f.close();
  size_t file_size = end - begin;

  std::ifstream in(file, std::ios::binary);
  std::vector<uint8_t> nibs;

  /* Read the nibcode */

  for (size_t i = 0; i < file_size; i++)
  {
    uint8_t nib = in.get();
    nibs.push_back((nib & 0xf0) >> 4);
    nibs.push_back(nib & 0xf);
  }

  /* Push it in the vec */

  std::vector<int> bytecode;
  for (unsigned i = 328 * 2; i < nibs.size(); i++)
    bytecode.push_back(nibs[i]);

  return bytecode;
}
