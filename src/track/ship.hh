#ifndef SHIP_HH
#define SHIP_HH
#include "decoder.hh"
#include "utils.hh"

#include <array>
#include <cmath>
#include <functional>

#define MEMORY_SIZE Data::data().get_memory_size()
#define CHECKPOINTS_PER_LAP Data::data().get_check_per_laps()
#define CHECKPOINT_DELAY Data::data().get_check_delay()
#define BLUE_ARROW_SPACING Data::data().get_blue_arrow_spacing()

class Track;

struct byte
{
  unsigned int b0_7 : 8;
  unsigned int b8_15 : 8;
};

union lc_byte {
  struct byte b;
  unsigned int full_byte;
};

class Ship
{
public:
  Ship(Track* track, int id);
  void init();
  void make_forked(int id);
  /* Pipeline */
  void update();
  int fetch();
  int decode();
  int execute();

  /* Helpers */
  void advance(int n);
  int load_inst();
  bool validate_check(int cycles);

  /* Instructions */
  void add(std::vector<int> nibbles);
  void or_inst(std::vector<int> nibbles);
  void xor_inst(std::vector<int> nibbles);
  void not_inst(std::vector<int> nibbles);
  void rol_inst(std::vector<int> nibbles);
  void asr_inst(std::vector<int> nibbles);
  void neg_inst(std::vector<int> nibbles);
  void mov_inst(std::vector<int> nibbles);
  void sub(std::vector<int> nibbles);
  void swp(std::vector<int> nibbles);
  void ldr(std::vector<int> nibbles);
  void addi(std::vector<int> nibbles);
  void str(std::vector<int> nibbles);
  void ldb(std::vector<int> nibbles);

  void crash(std::vector<int> nibbles);
  void check(std::vector<int> nibbles);
  void fork(std::vector<int> nibbles);
  void lc(std::vector<int> nibbles);
  void ll(std::vector<int> nibbles);
  void nop(std::vector<int> nibbles);
  void stat(std::vector<int> nibbles);
  void write(std::vector<int> nibbles);
  void and_inst(std::vector<int> nibbles);
  void cmp(std::vector<int> nibbles);
  void cmpi(std::vector<int> nibbles);

  void b(std::vector<int> nibbles);
  void bz(std::vector<int> nibbles);
  void bnz(std::vector<int> nibbles);
  void bs(std::vector<int> nibbles);

private:
  void set_ZS(int value);
  void store_buffer(int addr, int nib);

private:
  Track* track_;
  std::array<short, 18> regs_;
  Decoder decoder_;
  std::array<std::function<int(Ship&)>, 4> pipeline_;
  unsigned int pipidx_;
  int checks_; /* Checks' number */
  int check_timer_;
  int distance;
  int id_;
  int opcode_;
  int pc;
  int wait_counter_;
  int executing_ = 0;
  /* Feisar, Qirex... */
  int mode_;
  std::array<short, 64> buffer_;
};

#endif
