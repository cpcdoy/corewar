#include "track.hh"
#include "data.hh"
#include "logger.hh"
#include "ship.hh"

#include <assert.h>
#include <iostream>
#include <iterator>
#include <tuple>

Track::Track() : ships_()
{
  cycles_ = 0;
  id_ = 0;
  mem_ = std::vector<int>(Data::data().get_memory_size());
  mem_.resize(Data::data().get_memory_size());
  /* Can't call fork on those */
  ships_.emplace(std::piecewise_construct, std::make_tuple(id_),
                 std::make_tuple(this, id_));
  ships_.find(id_)->second.init();
  id_++;
  Logger::log().notify(ships_);
}

void Track::fork(int id)
{
  forks_.push_back(id);
}

void Track::fork_ships()
{
  if (!forks_.empty())
  {
    for (auto id : forks_)
    {
      ships_.emplace(std::piecewise_construct, std::make_tuple(id_),
                     std::make_tuple(ships_.find(id)->second));
      ships_.find(id_)->second.make_forked(id_);
      id_++;
    }
    Logger::log().notify(ships_);
    forks_.clear();
  }
}

void Track::crash(int id)
{
  crashes_.push_back(id);
}

void Track::crash_ships()
{
  if (!crashes_.empty())
  {
    for (auto id : crashes_)
      ships_.erase(ships_.find(id));
    Logger::log().notify(ships_);
    crashes_.clear();
  }
}

int Track::fetch(int n)
{
  /* Modulo should be done in calling function */
  assert(n < Data::data().get_memory_size());
  return mem_.at(n);
}

void Track::run()
{
  while (!ships_.empty())
  {
    Logger::log().cycle_count(cycles_);
    for (auto& ship : ships_)
      if (!ship.second.validate_check(cycles_))
        crash(ship.first);
    crash_ships();
    for (auto& ship : ships_)
      ship.second.update();
    Logger::log().print();
    crash_ships();
    fork_ships();
    cycles_++;
  }
  if (Logger::log().get_verbose() >= 3)
    std::cout << "Track runned " << cycles_ << " cycles.\nHah, losers.\n";
}

void Track::load(std::vector<int> bytecode)
{
  for (unsigned int i = 0; i < bytecode.size(); ++i)
  {
    mem_[i] = bytecode[i];
  }
  if (Logger::log().get_verbose() >= 3)
    std::cout << "Ships loaded. All systems nominal.\n";
}

int Track::get_cycles()
{
  return cycles_;
}

void Track::dec_cycles()
{
  cycles_--;
}

void Track::store_in_mem(int addr, int nib)
{
  mem_[addr % mem_.size()] = nib;
}
