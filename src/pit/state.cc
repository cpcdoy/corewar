#include "state.hh"
#include "utils.hh"

namespace states
{
  pass parser_state::get_state()
  {
    return s_;
  }

  void parser_state::set_state(pass curr_state)
  {
    s_ = curr_state;
  }

  void parser_state::store_label(const pegtl::action_input& in,
                                 std::string name, long int addr)
  {
    auto r = str_labels_.insert(name);
    if (r.second)
      labels_.insert(label(name, addr));
    else
      utils::error(in, name,
                   std::string("Duplicate label \"" + name + "\"").c_str());
  }

  unsigned long parser_state::get_label_addr(std::string name)
  {
    for (auto i = labels_.begin(); i != labels_.end(); i++)
    {
      auto val = *i;
      if (val.first == name)
        return val.second;
    }
    return -1;
  }

  bool parser_state::label_exists(std::string name)
  {
    for (auto i = labels_.begin(); i != labels_.end(); i++)
    {
      auto val = *i;
      if (val.first == name)
        return true;
    }
    return false;
  }

  unsigned long int parser_state::get_current_mem_addr()
  {
    return pc_;
  }

  void parser_state::incr_curr_mem_addr(unsigned long int inc)
  {
    pc_ += inc;
  }

  void parser_state::reset_curr_mem_addr()
  {
    pc_ = 0UL;
  }

  int parser_state::get_errors()
  {
    return errors_;
  }

  void parser_state::incr_err()
  {
    if (errors_ < 127)
      errors_++;
  }
}
