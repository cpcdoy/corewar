#pragma once

#include "include/pegtl.hh"

namespace utils
{
  void warn(const pegtl::action_input& in, std::string msg);
  void warn(const pegtl::action_input& in, const char* msg);
  void error(const pegtl::action_input& in, std::string nb, const char* msg);
  void number_warn(const pegtl::action_input& in, std::string nb_str, long nb,
                   int nibble_size, bool neg);
}
