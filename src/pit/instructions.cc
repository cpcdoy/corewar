#include "instructions.hh"
#include "data.h"
#include "state.hh"
#include "utils.hh"
#include <iostream>
#include <map>

namespace utils
{
  unsigned long int inst_size_imm(inst_size s)
  {
    return static_cast<unsigned long int>(s);
  }

  unsigned int get_mode(std::string m)
  {
    static std::map<std::string, unsigned int> modes;

    if (modes.empty())
    {
      modes.insert(std::make_pair("feisar", 0));
      modes.insert(std::make_pair("goteki45", 1));
      modes.insert(std::make_pair("agsystems", 2));
      modes.insert(std::make_pair("auricom", 3));
      modes.insert(std::make_pair("assegai", 4));
      modes.insert(std::make_pair("piranha", 5));
      modes.insert(std::make_pair("qirex", 6));
      modes.insert(std::make_pair("icaras", 7));
      modes.insert(std::make_pair("rocket", 8));
      modes.insert(std::make_pair("missile", 9));
      modes.insert(std::make_pair("mine", 10));
      modes.insert(std::make_pair("plasma", 11));
      modes.insert(std::make_pair("miniplasma", 12));
    }

    return modes[m];
  }

  char mask_int_to_nibble(unsigned long int n)
  {
    return n & 0xf;
  }

  bool is_alnum(std::string s)
  {
    const char* c = s.c_str();
    for (unsigned int i = 0; i < s.size(); i++)
      if (!std::isalnum(c[i]) && c[i] != '_')
        return false;
    return true;
  }

  unsigned long next_divisor(const pegtl::action_input& in, unsigned long at,
                             unsigned long align)
  {
    if (align == 0)
    {
      utils::error(in, std::string("0"), "Cannot align with 0 ! (FIX IT)");
      return at;
    }
    for (int i = at; i < MAX_CODE_SIZE; i++)
      if (i % align == 0)
        return i;

    utils::warn(in, "Cannot align to this multiple");

    return at;
  }

  std::string dilate_operators(std::string s)
  {
    s = output_stream::asm_stream::replace_all(s, "+", " + ");
    s = output_stream::asm_stream::replace_all(s, "-", " - ");
    s = output_stream::asm_stream::replace_all(s, "*", " * ");
    s = output_stream::asm_stream::replace_all(s, "/", " / ");
    s = output_stream::asm_stream::replace_all(s, "%", " % ");

    s = output_stream::asm_stream::replace_all(s, "(", " ( ");
    s = output_stream::asm_stream::replace_all(s, ")", " ) ");

    return s;
  }

  std::string smooth_op_exp(std::string s)
  {
    s = dilate_operators(s);

    std::vector<std::string> v;
    std::stringstream string_stream(s);
    std::string line;

    while (std::getline(string_stream, line))
    {
      std::size_t prev = 0;
      std::size_t pos;
      while ((pos = line.find_first_of("+-*/%()", prev)) != std::string::npos)
      {
        if (pos > prev)
          v.push_back(line.substr(prev, pos - prev));
        prev = pos + 1;
        v.push_back(line.substr(pos, 1));
      }
      if (prev < line.length())
        v.push_back(line.substr(prev, std::string::npos));
    }

    std::string r;
    for (unsigned int i = 0; i < v.size(); i++)
    {
      v[i] = output_stream::asm_stream::replace_all(v[i], " ", "");
      v[i] = output_stream::asm_stream::replace_all(v[i], "\n", "");
      v[i] = output_stream::asm_stream::replace_all(v[i], "\t", "");
      if (!states::parser_state::instance().label_exists(v[i]))
        v[i] = output_stream::asm_stream::replace_all(v[i], "_", "");
      r += v[i];
    }
    return r;
  }
}
