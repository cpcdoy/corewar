#pragma once

#include "calc_grammar.hh"
#include "include/pegtl.hh"
#include "output_stream.hh"
#include <iostream>

namespace casm
{
  /* Base blocks */

  struct comment : pegtl::if_must<pegtl::one<'#'>, pegtl::until<pegtl::eolf>>
  {
  };

  struct ignored : pegtl::sor<pegtl::plus<pegtl::space>, comment>
  {
  };

  struct beg_sep : pegtl::plus<pegtl::space>
  {
  };

  struct sep : pegtl::star<pegtl::space>
  {
  };

  struct reg_digit
      : pegtl::sor<pegtl::one<'0'>, pegtl::one<'1'>, pegtl::one<'2'>,
                   pegtl::one<'3'>, pegtl::one<'4'>, pegtl::one<'5'>>
  {
  };

  struct reg_num
      : pegtl::sor<pegtl::seq<pegtl::one<'1'>, reg_digit>, pegtl::digit>
  {
  };

  struct reg : pegtl::seq<pegtl::one<'r'>, reg_num>
  {
  };

  struct reg_val : pegtl::seq<pegtl::one<'['>, reg, pegtl::one<']'>>
  {
  };

  struct add_express : pegtl::seq<pegtl::one<'['>, reg, pegtl::one<']'>>
  {
  };

  struct ship_name
      : pegtl::seq<
            pegtl_string_t(".name"), beg_sep,
            pegtl::if_must<pegtl::one<'"'>, pegtl::until<pegtl::one<'"'>>>>
  {
  };

  struct ship_comment
      : pegtl::seq<
            pegtl_string_t(".comment"), beg_sep,
            pegtl::if_must<pegtl::one<'"'>, pegtl::until<pegtl::one<'"'>>>>
  {
  };

  //
  struct binary;
  struct hexa;
  struct octal;
  struct num;

  struct align : pegtl::seq<pegtl_string_t(".align"), beg_sep,
                            pegtl::sor<binary, hexa, octal, num>>
  {
  };

  /* numbers */

  struct num_stop
      : pegtl::sor<pegtl::space,
                   pegtl::one<'%', '+', '-', '*', '/', '(', ')', ','>>
  {
  };

  struct num_binary : pegtl::sor<pegtl::one<'0'>, pegtl::one<'1'>>
  {
  };

  struct binary
      : pegtl::seq<pegtl::one<'%'>, pegtl::must<num_binary>,
                   pegtl::must<pegtl::until<
                       pegtl::at<num_stop>,
                       pegtl::sor<num_binary,
                                  pegtl::if_must<pegtl::plus<pegtl::one<'_'>>,
                                                 num_binary>>>>>
  {
  };

  struct num_octal : pegtl::one<'0', '1', '2', '3', '4', '5', '6', '7'>
  {
  };

  struct octal
      : pegtl::seq<pegtl::one<'0'>, num_octal,
                   pegtl::must<pegtl::until<
                       pegtl::at<num_stop>,
                       pegtl::sor<num_octal,
                                  pegtl::if_must<pegtl::plus<pegtl::one<'_'>>,
                                                 num_octal>>>>>
  {
  };

  struct hexa
      : pegtl::seq<pegtl_string_t("0x"), pegtl::must<pegtl::xdigit>,
                   pegtl::must<pegtl::until<
                       pegtl::at<num_stop>,
                       pegtl::sor<pegtl::xdigit,
                                  pegtl::if_must<pegtl::plus<pegtl::one<'_'>>,
                                                 pegtl::xdigit>>>>>
  {
  };
  struct num
      : pegtl::seq<pegtl::digit,
                   pegtl::must<pegtl::until<
                       pegtl::at<num_stop>,
                       pegtl::sor<pegtl::digit,
                                  pegtl::if_must<pegtl::plus<pegtl::one<'_'>>,
                                                 pegtl::digit>>>>>
  {
  };

  struct label_name;

  struct value : pegtl::sor<binary, hexa, octal, num, label_name>
  {
  };

  /* Arithmetic rules */

  /*
      struct number
           : pegtl::sor<name, pegtl::seq<pegtl::opt<num_op>,
                value>> {};

      struct expression;

      struct bracket
           : pegtl::if_must< pegtl::one< '(' >,
             expression, pegtl::one< ')' > > {};

      struct atomic
           : pegtl::sor< number, num_op, bracket > {};

      struct expression
           : pegtl::list< atomic, sep> {};*/

  struct add_exp;

  struct terminal : pegtl::sor<pegtl::seq<sep, value>,
                               pegtl::seq<sep, pegtl::one<'('>, sep, add_exp,
                                          sep, pegtl::one<')'>, sep>>
  {
  };

  /*struct unary
      : pegtl::seq<pegtl::star<pegtl::one<'-', '+'>>,
               value> {};

  struct mul_exp
      : pegtl::seq<unary,
                   pegtl::star<pegtl::sor<pegtl::one<'*'>,
                                          pegtl::one<'/'>,
                                          pegtl::one<'%'>>,
                               mul_exp>> {};

  struct add_exp
      : pegtl::seq<mul_exp,
                   pegtl::star<pegtl::sor<pegtl::one<'+'>,
                                          pegtl::one<'-'>>,
                               add_exp>> {};*/
  struct fact;

  struct num_op : pegtl::one<'%', '+', '-', '*', '/', '(', ')'>
  {
  };

  struct first_op : pegtl::one<'+', '-', '('>
  {
  };

  struct op_exp_rec
      : pegtl::seq<sep, pegtl::opt<pegtl::plus<pegtl::seq<sep, num_op>>, sep,
                                   value, op_exp_rec>>
  {
  };

  struct op_exp2
      : pegtl::seq<sep, pegtl::star<first_op>, sep, pegtl::must<value>,
                   op_exp_rec, pegtl::star<num_op>>
  {
  };

  struct op : pegtl::one<'%', '+', '-', '*', '/'>
  {
  };

  struct op_exp;

  struct pm_num
      : pegtl::seq<pegtl::star<pegtl::sor<pegtl::one<'+', '-'>, pegtl::space>>,
                   pegtl::sor<pegtl::if_must<pegtl::one<'('>, sep, op_exp, sep,
                                             pegtl::one<')'>>,
                              value>>
  {
  };

  struct op_exp
      : pegtl::seq<pm_num, sep, pegtl::opt<pegtl::if_must<op, sep, op_exp>>>
  {
  };

  /* Labels */

  struct label_name
      : pegtl::seq<
            pegtl::alpha,
            pegtl::star<pegtl::seq<pegtl::star<pegtl::one<'_'>>, pegtl::alnum>>>
  {
  };

  struct label : pegtl::seq<label_name, pegtl::one<':'>>
  {
  };

  /* Instructions */

  /* Logical Instructions */

  struct and_inst
      : pegtl::seq<pegtl::if_must<pegtl::seq<pegtl_string_t("and"), beg_sep>,
                                  reg, sep, pegtl::one<','>, sep, reg>>
  {
  };

  struct or_inst
      : pegtl::seq<pegtl::if_must<pegtl::seq<pegtl_string_t("or"), beg_sep>,
                                  reg, sep, pegtl::one<','>, sep, reg>>
  {
  };

  struct not_inst
      : pegtl::seq<pegtl::if_must<pegtl::seq<pegtl_string_t("not"), beg_sep>,
                                  reg, sep, pegtl::one<','>, sep, reg>>
  {
  };
  struct rol_inst
      : pegtl::seq<pegtl::if_must<pegtl::seq<pegtl_string_t("rol"), beg_sep>,
                                  reg, sep, pegtl::one<','>, sep, op_exp>>
  {
  };

  struct xor_inst
      : pegtl::seq<pegtl::if_must<pegtl::seq<pegtl_string_t("xor"), beg_sep>,
                                  reg, sep, pegtl::one<','>, sep, reg>>
  {
  };

  /* Arithmetic Instructions */

  struct add_inst
      : pegtl::seq<pegtl::if_must<pegtl::seq<pegtl_string_t("add"), beg_sep>,
                                  reg, sep, pegtl::one<','>, sep, reg>>
  {
  };

  struct sub_inst
      : pegtl::seq<pegtl::if_must<pegtl::seq<pegtl_string_t("sub"), beg_sep>,
                                  reg, sep, pegtl::one<','>, sep, reg>>
  {
  };

  struct cmp_inst
      : pegtl::seq<pegtl::if_must<pegtl::seq<pegtl_string_t("cmp"), beg_sep>,
                                  reg, sep, pegtl::one<','>, sep, reg>>
  {
  };

  struct neg_inst
      : pegtl::seq<pegtl::if_must<pegtl::seq<pegtl_string_t("neg"), beg_sep>,
                                  reg, sep, pegtl::one<','>, sep, reg>>
  {
  };

  struct mov_inst
      : pegtl::seq<pegtl::if_must<pegtl::seq<pegtl_string_t("mov"), beg_sep>,
                                  reg, sep, pegtl::one<','>, sep, reg>>
  {
  };

  struct swp_inst
      : pegtl::seq<pegtl::if_must<pegtl::seq<pegtl_string_t("swp"), beg_sep>,
                                  reg, sep, pegtl::one<','>, sep, reg>>
  {
  };

  struct asr_inst
      : pegtl::seq<pegtl::if_must<pegtl::seq<pegtl_string_t("asr"), beg_sep>,
                                  reg, sep, pegtl::one<','>, sep, op_exp>>
  {
  };

  struct addi_inst
      : pegtl::seq<pegtl::if_must<pegtl::seq<pegtl_string_t("addi"), beg_sep>,
                                  reg, sep, pegtl::one<','>, sep, op_exp>>
  {
  };

  struct cmpi_inst
      : pegtl::seq<pegtl::if_must<pegtl::seq<pegtl_string_t("cmpi"), beg_sep>,
                                  reg, sep, pegtl::one<','>, sep, op_exp>>
  {
  };

  struct lc_inst
      : pegtl::seq<pegtl::if_must<pegtl::seq<pegtl_string_t("lc"), beg_sep>,
                                  reg, sep, pegtl::one<','>, sep, op_exp>>
  {
  };

  struct ll_inst
      : pegtl::seq<pegtl::if_must<pegtl::seq<pegtl_string_t("ll"), beg_sep>,
                                  reg, sep, pegtl::one<','>, sep, op_exp>>
  {
  };

  /* memory instruction */

  struct ldr_inst
      : pegtl::seq<pegtl::if_must<pegtl::seq<pegtl_string_t("ldr"), beg_sep>,
                                  reg, sep, pegtl::one<','>, sep, reg_val>>
  {
  };

  struct str_inst
      : pegtl::seq<pegtl::if_must<pegtl::seq<pegtl_string_t("str"), beg_sep>,
                                  reg_val, sep, pegtl::one<','>, sep, reg>>
  {
  };

  struct ldb_inst
      : pegtl::seq<pegtl::if_must<pegtl::seq<pegtl_string_t("ldb"), beg_sep>,
                                  reg_val, sep, pegtl::one<','>, sep, op_exp,
                                  sep, pegtl::one<','>, sep, op_exp>>
  {
  };

  struct stb_inst
      : pegtl::seq<pegtl::if_must<pegtl::seq<pegtl_string_t("stb"), beg_sep>,
                                  reg_val, sep, pegtl::one<','>, sep, op_exp,
                                  sep, pegtl::one<','>, sep, op_exp>>
  {
  };

  /* branch instruction */

  struct b_inst
      : pegtl::seq<
            pegtl::if_must<pegtl::seq<pegtl_string_t("b"), beg_sep>, reg>>
  {
  };

  struct bz_inst
      : pegtl::seq<
            pegtl::if_must<pegtl::seq<pegtl_string_t("bz"), beg_sep>, reg>>
  {
  };

  struct bnz_inst
      : pegtl::seq<
            pegtl::if_must<pegtl::seq<pegtl_string_t("bnz"), beg_sep>, reg>>
  {
  };

  struct bs_inst
      : pegtl::seq<
            pegtl::if_must<pegtl::seq<pegtl_string_t("bs"), beg_sep>, reg>>
  {
  };

  /* system instruction */
  struct crash_inst : pegtl::seq<pegtl_string_t("crash")>
  {
  };

  struct write_inst
      : pegtl::seq<
            pegtl::if_must<pegtl::seq<pegtl_string_t("write"), beg_sep>, reg>>
  {
  };

  struct stat_inst
      : pegtl::seq<pegtl::if_must<pegtl::seq<pegtl_string_t("stat"), beg_sep>,
                                  reg, sep, pegtl::one<','>, sep, op_exp>>
  {
  };

  struct check_inst : pegtl::seq<pegtl_string_t("check")>
  {
  };

  /* mode */

  struct ship
      : pegtl::sor<pegtl_istring_t("feisar"), pegtl_istring_t("goteki45"),
                   pegtl_istring_t("agsystems"), pegtl_istring_t("auricom"),
                   pegtl_istring_t("assegai"), pegtl_istring_t("piranha"),
                   pegtl_istring_t("qirex"), pegtl_istring_t("icaras"),
                   pegtl_istring_t("rocket"), pegtl_istring_t("missile"),
                   pegtl_istring_t("mine"), pegtl_istring_t("plasma"),
                   pegtl_istring_t("miniplasma")>
  {
  };

  struct mode_inst : pegtl::seq<pegtl_string_t("mode"), beg_sep, ship>
  {
  };

  struct fork_inst : pegtl::seq<pegtl_string_t("fork")>
  {
  };

  struct nop_inst : pegtl::seq<pegtl_string_t("nop")>
  {
  };

  struct rule_list;

  struct allowed_rules
      : pegtl::until<
            pegtl::eof,
            pegtl::sor<ignored, label, add_inst, addi_inst, and_inst, asr_inst,
                       b_inst, bnz_inst, bs_inst, bz_inst, check_inst, cmp_inst,
                       cmpi_inst, crash_inst, fork_inst, lc_inst, ldb_inst,
                       ldr_inst, ll_inst, mode_inst, mov_inst, neg_inst,
                       nop_inst, not_inst, or_inst, rol_inst, stat_inst,
                       stb_inst, str_inst, sub_inst, swp_inst, write_inst,
                       xor_inst, ship_name, ship_comment>>
  {
  };

  struct error_eater
      : pegtl::seq<pegtl::opt<pegtl::raise<rule_list>>,
                   pegtl::until<pegtl::at<allowed_rules>, pegtl::any>>
  {
  };

  /* entry point */
  struct rule_list
      : pegtl::until<
            pegtl::eof,
            pegtl::sor<ignored, label, add_inst, addi_inst, and_inst, asr_inst,
                       b_inst, bnz_inst, bs_inst, bz_inst, check_inst, cmp_inst,
                       cmpi_inst, crash_inst, fork_inst, lc_inst, ldb_inst,
                       ldr_inst, ll_inst, mode_inst, mov_inst, neg_inst,
                       nop_inst, not_inst, or_inst, rol_inst, stat_inst,
                       stb_inst, str_inst, sub_inst, swp_inst, write_inst,
                       xor_inst, align, ship_name, ship_comment, error_eater>>
  {
  };
}
