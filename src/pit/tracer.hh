#pragma once

#include "grammar.hh"
#include "include/pegtl.hh"

namespace casm
{
  template <typename Rule>
  struct error_control : public pegtl::normal<Rule>
  {
    template <typename Input, typename... States>
    static void start(const Input&, States&&...)
    {
    }

    template <typename Input, typename... States>
    static void success(const Input&, States&&...)
    {
    }

    template <typename Input, typename... States>
    static void failure(const Input&, States&&...)
    {
    }

    static const std::string error_message();

    template <typename Input, typename... States>
    static void raise(const Input& in, States&&...);
  };

  template <typename T>
  template <typename Input, typename... States>
  void error_control<T>::raise(const Input& in, States&&...)
  {

    size_t char_pos = in.byte_in_line();

    std::ostringstream o;
    o << "\x1b[31m"
      << "error   " << pegtl::position_info(in) << "\x1b[0m"
      << "\n    ";
    auto lines = output_stream::asm_stream::split(in.begin() - char_pos, '\n');
    std::string line = lines.size() ? lines[0] : in.begin();

    o << line << std::endl;
    if (line.length() < 80)
    {
      for (size_t i = 0; i < char_pos + 4; i++)
        o << " ";
      o << "\x1b[31m^\x1b[0m\n";
      if (char_pos + 4 > error_message().length() / 2)
      {
        for (size_t i = 0; i < char_pos + 4 - error_message().length() / 2; i++)
          o << " ";
      }
      else
      {
        for (size_t i = 0; i < char_pos + 4; i++)
          o << " ";
        o << "\x1b[31m^\x1b[0m\n";
      }
      o << error_message();
    }
    std::cerr << o.str() << std::endl;

    states::parser_state::instance().incr_err();
  }

  template <>
  template <typename Input, typename... States>
  void error_control<pegtl::ascii::one<(char)41>>::raise(const Input& in,
                                                         States&&...)
  {
    std::ostringstream o;
    o << "\x1b[31m"
      << "error   " << in.source() << ':' << in.line() - 1 << ':'
      << in.byte_in_line() << "\x1b[0m"
      << "\n    " << error_message();
    std::cerr << o.str() << std::endl;

    states::parser_state::instance().incr_err();
  }

  template <typename T>
  const std::string error_control<T>::error_message()
  {
    return "error " + pegtl::internal::demangle<T>();
  }

  template <>
  inline const std::string
  error_control<pegtl::ascii::one<(char)41>>::error_message()
  {
    return "missing ')'";
  }

  template <>
  inline const std::string casm::error_control<
      pegtl::until<pegtl::ascii::one<(char)34>>>::error_message()
  {
    return "missing double quote";
  }

  template <>
  inline const std::string error_control<pegtl::ascii::xdigit>::error_message()
  {
    return "expecting hexa number";
  }

  template <>
  inline const std::string error_control<pegtl::ascii::digit>::error_message()
  {
    return "expecting digit";
  }

  template <>
  inline const std::string error_control<pegtl::one<(char)44>>::error_message()
  {
    return "missing comma";
  }

  template <>
  inline const std::string error_control<reg>::error_message()
  {
    return "expecting register ";
  }

  template <>
  inline const std::string error_control<reg_val>::error_message()
  {
    return "expecting [register]";
  }

  template <>
  inline const std::string error_control<op_exp>::error_message()
  {
    return "expecting value";
  }

  template <>
  inline const std::string error_control<rule_list>::error_message()
  {
    return "unknown instruction";
  }

  template <>
  inline const std::string error_control<num_octal>::error_message()
  {
    return "wrong octal number";
  }

  template <>
  inline const std::string error_control<num_binary>::error_message()
  {
    return "wrong binary number";
  }

  template <>
  inline const std::string error_control<hexa>::error_message()
  {
    return "wrong hexa number";
  }
}

