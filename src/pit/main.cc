#include "parser.hh"
#include <iostream>

int main(int argc, char** argv)
{
  return parser::parse_args(argc, argv);
}
