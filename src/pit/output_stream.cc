#include "output_stream.hh"

namespace output_stream
{
  void asm_stream::flush()
  {
    if (nibbles_in_stream_ % 2 != 0)
      buffered_stream_ << current_byte_;
    stream_ << buffered_stream_.str();
  }

  asm_stream& asm_stream::operator<<(const char& c)
  {
    if (nibbles_in_stream_ > 0 && nibbles_in_stream_ % 2 != 0)
    {
      current_byte_ |= c;
      buffered_stream_ << current_byte_;
    }
    else
    {
      current_byte_ = c;
      current_byte_ <<= 4;
    }
    nibbles_in_stream_++;
    return *this;
  }

  std::ofstream& asm_stream::get_stream()
  {
    return stream_;
  }

  std::stringstream& asm_stream::get_buffered_stream()
  {
    return buffered_stream_;
  }

  void asm_stream::set_ship_name(std::string name)
  {
    ship_name_ = name;
  }

  std::string& asm_stream::get_ship_name()
  {
    return ship_name_;
  }

  void asm_stream::set_ship_comment(std::string comment)
  {
    ship_comment_ = comment;
  }

  std::string& asm_stream::get_ship_comment()
  {
    return ship_comment_;
  }

  void asm_stream::write_output(bool b)
  {
    write_output_ = b;
  }

  bool asm_stream::write_output()
  {
    return write_output_;
  }
}
