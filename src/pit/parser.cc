#include "parser.hh"
#include "data.h"
#include "empty_tracer.hh"
#include "output_stream.hh"
#include "state.hh"
#include <string.h>

namespace parser
{
  void parse(char* file_path)
  {
    std::vector<std::string> strs;
    // First parsing pass (labels + comment and name)
    states::parser_state::instance().set_state(states::pass::pass1);
    pegtl::file_parser(file_path)
        .parse<casm::rule_list, casm::action, casm::error_control>();

    output_stream::asm_stream::split(file_path, '.', strs);
    if (output_stream::asm_stream::instance().write_output())
      output_stream::asm_stream::instance().get_stream().open(strs[0] + ".cor",
                                                              std::ios::binary);
    //.cor header
    std::stringstream ss;
    ss << std::hex << MAGIC_NUMBER;
    std::string hex = ss.str();
    int len = hex.length();
    std::string magic_number;
    for (int i = len - 2; i >= 0; i -= 2)
    {
      std::string byte = hex.substr(i, 2);
      char chr = (char)(int)strtol(byte.c_str(), nullptr, 16);
      magic_number.push_back(chr);
    }
    output_stream::asm_stream::instance().get_buffered_stream() << magic_number;
    // Set code size + padding
    long code_size = states::parser_state::instance().get_current_mem_addr();
    if (code_size > MAX_CODE_SIZE)
    {
      std::cerr << "Code size is too long (" << code_size
                << "), it should be under " << MAX_CODE_SIZE;

      abort();
    }

    // Code size in the header
    output_stream::asm_stream::instance().get_buffered_stream()
        << (char)code_size << (char)((code_size & 0xff00) >> 8);

    // Header padding
    output_stream::asm_stream::instance().get_buffered_stream() << '\0' << '\0';

    // Ship name
    output_stream::asm_stream::instance().get_buffered_stream()
        << output_stream::asm_stream::instance().get_ship_name().substr(
               0, NAME_LENGTH - 1);

    // Name padding
    unsigned int l = strlen(output_stream::asm_stream::instance()
                                .get_ship_name()
                                .substr(0, NAME_LENGTH - 1)
                                .c_str());
    if (l >= NAME_LENGTH)
      l = 0;
    else
      l = NAME_LENGTH - l;
    for (unsigned int i = 0; i < l; i++)
      output_stream::asm_stream::instance().get_buffered_stream() << '\0';

    // Ship comment
    output_stream::asm_stream::instance().get_buffered_stream()
        << output_stream::asm_stream::instance().get_ship_comment().substr(
               0, COMMENT_LENGTH - 1);

    // Comment padding
    l = strlen(output_stream::asm_stream::instance()
                   .get_ship_comment()
                   .substr(0, COMMENT_LENGTH - 1)
                   .c_str());
    if (l >= COMMENT_LENGTH)
      l = 0;
    else
      l = COMMENT_LENGTH - l;
    for (unsigned int i = 0; i < l; i++)
      output_stream::asm_stream::instance().get_buffered_stream() << '\0';

    // Reset for label counting
    states::parser_state::instance().reset_curr_mem_addr();

    // Second parsing pass (grammar)
    if (!states::parser_state::instance().get_errors())
    {
      states::parser_state::instance().set_state(states::pass::pass2);
      pegtl::file_parser(file_path)
          .parse<casm::rule_list, casm::action, casm::empty_tracer>();
    }

    output_stream::asm_stream::instance().flush();
    output_stream::asm_stream::instance().get_stream().close();

    if (!output_stream::asm_stream::instance().write_output() ||
        states::parser_state::instance().get_errors())
      std::remove((strs[0] + ".cor").c_str());
  }

  int help()
  {
    std::cout << "Usage : ./pit <.s file>" << std::endl;
    return 1;
  }

  int set_no_output()
  {
    output_stream::asm_stream::instance().write_output(false);
    return 0;
  }

  int parse_files(std::vector<char*>& files)
  {
    for (auto file : files)
    {
      try
      {
        parse(file);
      }
      catch (pegtl::input_error e)
      {
        std::cout << "Cannot open " << file[1] << std::endl;
      }
      catch (pegtl::parse_error e)
      {
        std::cerr << e.what() << std::endl;
      }
    }
    return states::parser_state::instance().get_errors();
  }

  int parse_args(int argc, char** argv)
  {
    static std::unordered_map<std::string, handler> args = {
        {"--help", help},
        {"-h", help},
        {"--no-write", set_no_output},
        {"-n", set_no_output}};

    if (argc > 1)
    {
      std::vector<char*> files;
      for (int i = 1; i < argc; i++)
      {
        auto res = args.find(argv[i]);
        if (res == args.end())
          files.push_back(argv[i]);
        else if (res->second())
          return 0;
      }
      return parse_files(files);
    }
    return help();
  }
}
