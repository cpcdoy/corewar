#pragma once

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

namespace output_stream
{
  class asm_stream
  {
  public:
    static asm_stream& instance()
    {
      static asm_stream stream;
      return stream;
    }

    static std::string replace_all(std::string str, const std::string& from,
                                   const std::string& to)
    {
      size_t start_pos = 0;
      while ((start_pos = str.find(from, start_pos)) != std::string::npos)
      {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length();
      }
      return str;
    }

    static std::vector<std::string>& split(const std::string& s, char delim,
                                           std::vector<std::string>& elems)
    {
      std::stringstream ss(s);
      std::string item;
      while (std::getline(ss, item, delim))
        elems.push_back(item);
      return elems;
    }

    static std::vector<std::string> split(const std::string& s, char delim)
    {
      std::vector<std::string> elems;
      split(s, delim, elems);
      for (unsigned int i = 0; i < elems.size(); i++)
        if (elems[i] == "" || elems[i] == " ")
        {
          elems.erase(elems.begin() + i);
          i = 0;
        }
      return elems;
    }

    void write_output(bool b);

    bool write_output();

    void flush();
    asm_stream& operator<<(const char& c);
    std::ofstream& get_stream();
    std::stringstream& get_buffered_stream();
    void set_ship_name(std::string name);
    std::string& get_ship_name();
    void set_ship_comment(std::string comment);
    std::string& get_ship_comment();

  private:
    std::ofstream stream_;
    std::stringstream buffered_stream_;
    std::string ship_name_;
    std::string ship_comment_;
    bool write_output_ = true;

    unsigned long int nibbles_in_stream_ = 0UL;
    unsigned char current_byte_ = 0;
  };
}
