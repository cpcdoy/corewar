#include "utils.hh"
#include "instructions.hh"
#include "output_stream.hh"

namespace utils
{
  void warn(const pegtl::action_input& in, std::string msg)
  {
    size_t char_pos = in.byte_in_line();
    std::ostringstream o;
    o << "\x1b[35m"
      << "warning   " << pegtl::position_info(in) << "\x1b[0m"
      << "\n    ";

    auto lines = output_stream::asm_stream::split(in.begin() - char_pos, '\n');
    std::string line = lines.size() ? lines[0] : in.begin();
    o << line << std::endl;
    if (line.length() < 80)
    {
      for (size_t i = 0; i < char_pos + 4; i++)
        o << " ";
      o << "\x1b[35m^\x1b[0m\n";
      if (char_pos + 4 > msg.length() / 2)
      {
        for (size_t i = 0; i < char_pos + 4 - msg.length() / 2; i++)
          o << " ";
      }
    }
    else
    {
      for (size_t i = 0; i < char_pos + 4; i++)
        o << " ";
      o << "\x1b[35m^\x1b[0m\n";
    }
    o << msg;
    std::cerr << o.str() << std::endl << std::endl;
  }

  static void error(const pegtl::action_input& in, std::string nb,
                    std::string msg)
  {
    size_t char_pos = in.byte_in_line();
    std::ostringstream o;
    o << "\x1b[31m"
      << "error   " << pegtl::position_info(in) << "\x1b[0m"
      << "\n    ";

    auto lines = output_stream::asm_stream::split(in.begin() - char_pos, '\n');
    std::string line = lines.size() ? lines[0] : in.begin();
    o << line << std::endl;
    if (line.length() < 80)
    {
      for (size_t i = 0; i < char_pos + 4; i++)
        o << " ";
      for (size_t i = 0; i < (char_pos > 0 ? nb.size() : 1); i++)
        o << "\x1b[31m^\x1b[0m";
      o << std::endl;
      if (char_pos + 4 > msg.length() / 2)
      {
        for (size_t i = 0; i < char_pos + 4 - msg.length() / 2; i++)
          o << " ";
      }
    }
    else
    {
      for (size_t i = 0; i < char_pos + 4; i++)
        o << " ";
      o << "\x1b[31m^\x1b[0m\n";
    }
    o << msg;
    std::cerr << o.str() << std::endl << std::endl;
  }

  void error(const pegtl::action_input& in, std::string nb, const char* msg)
  {
    std::string s = msg;
    error(in, nb, s);
  }

  void warn(const pegtl::action_input& in, std::string nb, std::string msg)
  {
    size_t char_pos = in.string().find(nb);
    if (char_pos == std::string::npos)
      char_pos = 0;

    std::ostringstream o;
    o << "\x1b[35m"
      << "warning   " << pegtl::position_info(in) << "\x1b[0m"
      << "\n    ";

    auto lines = output_stream::asm_stream::split(in.begin(), '\n');
    std::string line = lines.size() ? lines[0] : in.begin();
    o << line << std::endl;
    if (line.length() < 80)
    {
      for (size_t i = 0; i < char_pos + 4; i++)
        o << " ";
      for (size_t i = 0; i < (char_pos > 0 ? nb.size() : 1); i++)
        o << "\x1b[35m^\x1b[0m";
      o << std::endl;
      if (char_pos + 4 > msg.length() / 2)
      {
        for (size_t i = 0; i < char_pos + 4 - msg.length() / 2; i++)
          o << " ";
      }
    }
    else
    {
      for (size_t i = 0; i < char_pos + 4; i++)
        o << " ";
      o << "\x1b[35m^\x1b[0m";
      o << std::endl;
    }
    o << msg;
    std::cerr << o.str() << std::endl << std::endl;
  }

  void warn(const pegtl::action_input& in, const char* msg)
  {
    std::string s = msg;
    warn(in, s);
  }

  void number_warn(const pegtl::action_input& in, std::string nb_str, long nb,
                   int nibble_size, bool neg)
  {
    if (!neg)
    {
      unsigned long n = nb;
      unsigned long r = 0;
      if (n > (1 << 4) && nibble_size == 1)
        r = utils::mask_int_to_nibble(n);
      else if (n > (1 << 8) && nibble_size == 2)
        r = n & 0x100;
      else if (n > (1 << 16) && nibble_size == 4)
        r = n & 0xffff;

      if (r != 0)
        warn(in, nb_str,
             std::string("Truncating \"" + nb_str + "\" (" +
                         std::to_string(nb) + ") to \"" + std::to_string(r) +
                         "\", it should be " + std::to_string(nibble_size) +
                         " nibble" + (nibble_size != 2 ? "s" : "")));
      if (nb_str[0] == '-')
        warn(in, nb_str, std::string("\"" + nb_str +
                                     "\" is negative and should not be. "
                                     "Using the signed representation : \"") +
                             std::to_string(r) + std::string("\""));
    }
    else
    {
      long r = 0;
      if (nb > (1 << 4) && nibble_size == 1)
        r = utils::mask_int_to_nibble(nb);
      else if (nb > (1 << 8) && nibble_size == 2)
        r = nb & 0x100;
      else if (nb > (1 << 16) && nibble_size == 4)
        r = nb & 0xffff;

      if (r != 0)
        warn(in, nb_str,
             std::string("Truncating \"" + nb_str + "\" (" +
                         std::to_string(nb) + ") to \"" + std::to_string(r) +
                         "\", it should be " + std::to_string(nibble_size) +
                         " nibble" + (nibble_size != 2 ? "s" : "")));
    }
  }
}
