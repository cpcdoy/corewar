#pragma once
#include "include/pegtl.hh"
#include "output_stream.hh"
#include <sstream>
#include <string>
#include <vector>

namespace utils
{
  enum opcodes
  {
    crash_ = 0b0000,
    nop_ = 0b0001,
    and_ = 0b0010,
    or_ = 0b0011,
    xor_ = 0b0100,
    rol_ = 0b0110,
    asr_ = 0b0111,
    not_ = 0b0101,
    add_ = 0b1000,
    sub_ = 0b1001,
    cmp_ = 0b1010,
    neg_ = 0b1011,
    mov_ = 0b1100,
    ldr_ = 0b1101,
    str_ = 0b1110,
    ext_ = 0b1111,
    swp_ = 0b0100,
    ldb_ = 0b0000,
    stb_ = 0b0001,
    lc_ = 0b0010,
    ll_ = 0b0011,
    addi_ = 0b0101,
    cmpi_ = 0b0110,
    b_ = 0b0111,
    bz_ = 0b1000,
    bnz_ = 0b1001,
    bs_ = 0b1010,
    stat_ = 0b1011,
    check_ = 0b1100,
    mode_ = 0b1101,
    fork_ = 0b1110,
    write_ = 0b1111
  };

  enum class inst_size : unsigned long int
  {
    crash_ = 1,
    nop_ = 1,
    and_ = 3,
    or_ = 3,
    xor_ = 3,
    rol_ = 3,
    asr_ = 3,
    not_ = 3,
    add_ = 3,
    sub_ = 3,
    cmp_ = 3,
    neg_ = 3,
    mov_ = 3,
    ldr_ = 3,
    str_ = 3,
    swp_ = 4,
    ldb_ = 7,
    stb_ = 7,
    lc_ = 5,
    ll_ = 7,
    addi_ = 4,
    cmpi_ = 4,
    b_ = 3,
    bz_ = 3,
    bnz_ = 3,
    bs_ = 3,
    stat_ = 4,
    check_ = 2,
    mode_ = 3,
    fork_ = 2,
    write_ = 3
  };

  unsigned long int inst_size_imm(inst_size s);
  unsigned int get_mode(std::string m);
  char mask_int_to_nibble(unsigned long int n);
  bool is_alnum(std::string s);
  unsigned long next_divisor(const pegtl::action_input& in, unsigned long at,
                             unsigned long align);
  std::string smooth_op_exp(std::string s);
}
