#pragma once

#include "grammar.hh"
#include "include/pegtl.hh"

namespace casm
{
  template <typename Rule>
  struct empty_tracer : public pegtl::normal<Rule>
  {
    template <typename Input, typename... States>
    static void start(const Input&, States&&...)
    {
    }

    template <typename Input, typename... States>
    static void success(const Input&, States&&...)
    {
    }

    template <typename Input, typename... States>
    static void failure(const Input&, States&&...)
    {
    }

    template <typename Input, typename... States>
    static void raise(const Input&, States&&...)
    {
    }
  };
}
