#pragma once
#include "data.h"
#include "grammar.hh"
#include "include/pegtl.hh"
#include "instructions.hh"
#include "output_stream.hh"
#include "state.hh"
#include "utils.hh"

namespace casm
{
  template <typename Rule>
  struct action : pegtl::nothing<Rule>
  {
  };

  template <>
  struct action<crash_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::crash_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      (void)in;
      output_stream::asm_stream::instance() << utils::opcodes::crash_;
    }
  };

  template <>
  struct action<nop_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::nop_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      (void)in;
      output_stream::asm_stream::instance() << utils::opcodes::nop_;
    }
  };

  template <>
  struct action<xor_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::xor_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      char* end;
      std::string s = in.string();
      s = output_stream::asm_stream::replace_all(s, " ", "");

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::xor_;

      char d;
      d = (char)std::strtol(ops[0].c_str() + 4, &end, 10);
      output_stream::asm_stream::instance() << d;

      d = (char)std::strtol(ops[1].c_str() + 1, &end, 10);
      output_stream::asm_stream::instance() << d;
    }
  };

  template <>
  struct action<add_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::add_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      char* end;
      std::string s = in.string();
      s = output_stream::asm_stream::replace_all(s, " ", "");

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::add_;

      char d;
      d = (char)std::strtol(ops[0].c_str() + 4, &end, 10);
      output_stream::asm_stream::instance() << d;

      d = (char)std::strtol(ops[1].c_str() + 1, &end, 10);
      output_stream::asm_stream::instance() << d;
    }
  };

  template <>
  struct action<addi_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::addi_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      char* end;
      std::string s = in.string();
      s = output_stream::asm_stream::replace_all(s, " ", "");
      s = output_stream::asm_stream::replace_all(s, "\n", "");
      s = output_stream::asm_stream::replace_all(s, "\t", "");

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::ext_;
      output_stream::asm_stream::instance() << utils::opcodes::addi_;

      char d;
      d = (char)std::strtol(ops[0].c_str() + 5, &end, 10);
      output_stream::asm_stream::instance() << d;

      calc::stacks stacks;
      calc::operators operators;
      ops[1] = utils::smooth_op_exp(ops[1]);
      pegtl::parse_string<calc::calc_grammar, calc::action>(ops[1], ops[1],
                                                            operators, stacks);

      auto r = stacks.finish();
      utils::number_warn(in, ops[1], r, 1, true);

      output_stream::asm_stream::instance() << utils::mask_int_to_nibble(r);
    }
  };

  template <>
  struct action<lc_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::lc_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      char* end;
      std::string s = in.string();
      s = output_stream::asm_stream::replace_all(s, " ", "");
      s = output_stream::asm_stream::replace_all(s, "\n", "");
      s = output_stream::asm_stream::replace_all(s, "\t", "");

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::ext_;
      output_stream::asm_stream::instance() << utils::opcodes::lc_;

      char d;
      d = (char)std::strtol(ops[0].c_str() + 3, &end, 10);
      output_stream::asm_stream::instance() << d;

      calc::stacks stacks;
      calc::operators operators;
      ops[1] = utils::smooth_op_exp(ops[1]);

      pegtl::parse_string<calc::calc_grammar, calc::action>(ops[1], ops[1],
                                                            operators, stacks);

      auto r = stacks.finish();
      utils::number_warn(in, ops[1], r, 2, true);

      output_stream::asm_stream::instance() << utils::mask_int_to_nibble(r)
                                            << ((r & 0xf0) >> 4);
    }
  };

  template <>
  struct action<ll_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::ll_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      char* end;
      std::string s = in.string();
      s = output_stream::asm_stream::replace_all(s, " ", "");
      s = output_stream::asm_stream::replace_all(s, "\n", "");
      s = output_stream::asm_stream::replace_all(s, "\t", "");

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::ext_;
      output_stream::asm_stream::instance() << utils::opcodes::ll_;

      char d;
      d = (char)std::strtol(ops[0].c_str() + 3, &end, 10);
      output_stream::asm_stream::instance() << d;

      calc::stacks stacks;
      calc::operators operators;
      ops[1] = utils::smooth_op_exp(ops[1]);
      pegtl::parse_string<calc::calc_grammar, calc::action>(ops[1], ops[1],
                                                            operators, stacks);

      auto r = stacks.finish();
      utils::number_warn(in, ops[1], r, 4, true);

      output_stream::asm_stream::instance()
          << utils::mask_int_to_nibble(r) << ((r & 0xf0) >> 4)
          << ((r & 0xf00) >> 8) << ((r & 0xf000) >> 12);
    }
  };

  template <>
  struct action<ldb_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::ldb_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      char* end;
      std::string s = in.string();
      s = output_stream::asm_stream::replace_all(s, " ", "");
      s = output_stream::asm_stream::replace_all(s, "\n", "");
      s = output_stream::asm_stream::replace_all(s, "\t", "");
      s = output_stream::asm_stream::replace_all(s, "[", "");
      s = output_stream::asm_stream::replace_all(s, "]", "");

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::ext_;
      output_stream::asm_stream::instance() << utils::opcodes::ldb_;

      char d;
      d = (char)std::strtol(ops[0].c_str() + 4, &end, 10);
      output_stream::asm_stream::instance() << d;

      calc::stacks stacks;
      calc::operators operators;
      ops[1] = utils::smooth_op_exp(ops[1]);
      pegtl::parse_string<calc::calc_grammar, calc::action>(ops[1], ops[1],
                                                            operators, stacks);

      auto r1 = stacks.finish();
      utils::number_warn(in, ops[1], r1, 2, false);

      ops[2] = utils::smooth_op_exp(ops[2]);
      pegtl::parse_string<calc::calc_grammar, calc::action>(ops[2], ops[2],
                                                            operators, stacks);
      auto r2 = stacks.finish();
      utils::number_warn(in, ops[2], r2, 2, false);

      output_stream::asm_stream::instance() << utils::mask_int_to_nibble(r1)
                                            << ((r1 & 0xf0) >> 4);
      output_stream::asm_stream::instance() << utils::mask_int_to_nibble(r2)
                                            << ((r2 & 0xf0) >> 4);
    }
  };

  template <>
  struct action<stb_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::stb_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      char* end;
      std::string s = in.string();
      s = output_stream::asm_stream::replace_all(s, " ", "");
      s = output_stream::asm_stream::replace_all(s, "\n", "");
      s = output_stream::asm_stream::replace_all(s, "\t", "");
      s = output_stream::asm_stream::replace_all(s, "[", "");
      s = output_stream::asm_stream::replace_all(s, "]", "");

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::ext_;
      output_stream::asm_stream::instance() << utils::opcodes::stb_;

      char d;
      d = (char)std::strtol(ops[0].c_str() + 4, &end, 10);
      output_stream::asm_stream::instance() << d;

      calc::stacks stacks;
      calc::operators operators;
      ops[1] = utils::smooth_op_exp(ops[1]);
      pegtl::parse_string<calc::calc_grammar, calc::action>(ops[1], ops[1],
                                                            operators, stacks);

      auto r1 = stacks.finish();
      utils::number_warn(in, ops[1], r1, 2, false);

      calc::stacks s2;
      calc::operators o2;
      ops[2] = utils::smooth_op_exp(ops[2]);
      pegtl::parse_string<calc::calc_grammar, calc::action>(ops[2], ops[2], o2,
                                                            s2);
      auto r2 = s2.finish();
      utils::number_warn(in, ops[2], r2, 2, false);

      output_stream::asm_stream::instance() << utils::mask_int_to_nibble(r1)
                                            << ((r1 & 0xf0) >> 4);
      output_stream::asm_stream::instance() << utils::mask_int_to_nibble(r2)
                                            << ((r2 & 0xf0) >> 4);
    }
  };

  template <>
  struct action<and_inst>
  {
    static void apply(const pegtl::action_input& in)
    {

      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::and_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      char* end;
      std::string s = in.string();
      s = output_stream::asm_stream::replace_all(s, " ", "");

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::and_;

      char d;
      d = (char)std::strtol(ops[0].c_str() + 4, &end, 10);
      output_stream::asm_stream::instance() << d;

      d = (char)std::strtol(ops[1].c_str() + 1, &end, 10);
      output_stream::asm_stream::instance() << d;
    }
  };

  template <>
  struct action<asr_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::asr_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      char* end;
      std::string s = in.string();
      s = output_stream::asm_stream::replace_all(s, " ", "");
      s = output_stream::asm_stream::replace_all(s, "\n", "");
      s = output_stream::asm_stream::replace_all(s, "\t", "");

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::asr_;

      char d;
      d = (char)std::strtol(ops[0].c_str() + 4, &end, 10);
      output_stream::asm_stream::instance() << d;

      calc::stacks stacks;
      calc::operators operators;
      ops[1] = utils::smooth_op_exp(ops[1]);
      pegtl::parse_string<calc::calc_grammar, calc::action>(ops[1], ops[1],
                                                            operators, stacks);

      auto r = stacks.finish();
      utils::number_warn(in, ops[1], r, 1, false);
      output_stream::asm_stream::instance() << utils::mask_int_to_nibble(r);
    }
  };

  template <>
  struct action<or_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::or_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      char* end;
      std::string s = in.string();
      s = output_stream::asm_stream::replace_all(s, " ", "");

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::or_;

      char d;
      d = (char)std::strtol(ops[0].c_str() + 3, &end, 10);
      output_stream::asm_stream::instance() << d;

      d = (char)std::strtol(ops[1].c_str() + 1, &end, 10);
      output_stream::asm_stream::instance() << d;
    }
  };

  template <>
  struct action<not_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::not_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      char* end;
      std::string s = in.string();
      s = output_stream::asm_stream::replace_all(s, " ", "");

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::not_;

      char d;
      d = (char)std::strtol(ops[0].c_str() + 4, &end, 10);
      output_stream::asm_stream::instance() << d;

      d = (char)std::strtol(ops[1].c_str() + 1, &end, 10);
      output_stream::asm_stream::instance() << d;
    }
  };

  template <>
  struct action<rol_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::rol_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      calc::stacks stacks;
      calc::operators operators;

      char* end;
      std::string s = in.string();

      s = output_stream::asm_stream::replace_all(s, " ", "");
      s = output_stream::asm_stream::replace_all(s, "\n", "");
      s = output_stream::asm_stream::replace_all(s, "\t", "");
      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::rol_;

      char d;
      d = (char)std::strtol(ops[0].c_str() + 4, &end, 10);
      output_stream::asm_stream::instance() << d;

      ops[1] = utils::smooth_op_exp(ops[1]);
      pegtl::parse_string<calc::calc_grammar, calc::action>(ops[1], ops[1],
                                                            operators, stacks);

      auto r = stacks.finish();
      utils::number_warn(in, ops[1], r, 1, false);
      output_stream::asm_stream::instance() << utils::mask_int_to_nibble(r);
    }
  };

  template <>
  struct action<sub_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::sub_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      char* end;
      std::string s = in.string();
      s = output_stream::asm_stream::replace_all(s, " ", "");

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::sub_;

      char d;
      d = (char)std::strtol(ops[0].c_str() + 4, &end, 10);
      output_stream::asm_stream::instance() << d;

      d = (char)std::strtol(ops[1].c_str() + 1, &end, 10);
      output_stream::asm_stream::instance() << d;
    }
  };

  template <>
  struct action<cmp_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::cmp_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      char* end;
      std::string s = in.string();
      s = output_stream::asm_stream::replace_all(s, " ", "");

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::cmp_;

      char d;
      d = (char)std::strtol(ops[0].c_str() + 4, &end, 10);
      output_stream::asm_stream::instance() << d;

      d = (char)std::strtol(ops[1].c_str() + 1, &end, 10);
      output_stream::asm_stream::instance() << d;
    }
  };

  template <>
  struct action<neg_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::neg_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      char* end;
      std::string s = in.string();
      s = output_stream::asm_stream::replace_all(s, " ", "");

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::neg_;

      char d;
      d = (char)std::strtol(ops[0].c_str() + 4, &end, 10);
      output_stream::asm_stream::instance() << d;

      d = (char)std::strtol(ops[1].c_str() + 1, &end, 10);
      output_stream::asm_stream::instance() << d;
    }
  };

  template <>
  struct action<mov_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::mov_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      char* end;
      std::string s = in.string();
      s = output_stream::asm_stream::replace_all(s, " ", "");

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::mov_;

      char d;
      d = (char)std::strtol(ops[0].c_str() + 4, &end, 10);
      output_stream::asm_stream::instance() << d;

      d = (char)std::strtol(ops[1].c_str() + 1, &end, 10);
      output_stream::asm_stream::instance() << d;
    }
  };

  template <>
  struct action<ldr_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::ldr_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      char* end;
      std::string s = in.string();
      s = output_stream::asm_stream::replace_all(s, "[", "");
      s = output_stream::asm_stream::replace_all(s, "]", "");
      s = output_stream::asm_stream::replace_all(s, " ", "");

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::ldr_;

      char d;
      d = (char)std::strtol(ops[0].c_str() + 4, &end, 10);
      output_stream::asm_stream::instance() << d;

      d = (char)std::strtol(ops[1].c_str() + 1, &end, 10);
      output_stream::asm_stream::instance() << d;
    }
  };

  template <>
  struct action<str_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::str_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      char* end;
      std::string s = in.string();
      s = output_stream::asm_stream::replace_all(s, "[", "");
      s = output_stream::asm_stream::replace_all(s, "]", "");
      s = output_stream::asm_stream::replace_all(s, " ", "");

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::str_;

      char d;
      d = (char)std::strtol(ops[0].c_str() + 4, &end, 10);
      output_stream::asm_stream::instance() << d;

      d = (char)std::strtol(ops[1].c_str() + 1, &end, 10);
      output_stream::asm_stream::instance() << d;
    }
  };

  template <>
  struct action<swp_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::swp_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      char* end;
      std::string s = in.string();
      s = output_stream::asm_stream::replace_all(s, " ", "");

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::ext_;
      output_stream::asm_stream::instance() << utils::opcodes::swp_;

      char d;
      d = (char)std::strtol(ops[0].c_str() + 4, &end, 10);
      output_stream::asm_stream::instance() << d;

      d = (char)std::strtol(ops[1].c_str() + 1, &end, 10);
      output_stream::asm_stream::instance() << d;
    }
  };

  template <>
  struct action<cmpi_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::cmpi_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      char* end;
      std::string s = in.string();
      s = output_stream::asm_stream::replace_all(s, " ", "");
      s = output_stream::asm_stream::replace_all(s, "\n", "");
      s = output_stream::asm_stream::replace_all(s, "\t", "");

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::ext_;
      output_stream::asm_stream::instance() << utils::opcodes::cmpi_;

      char d;
      d = (char)std::strtol(ops[0].c_str() + 5, &end, 10);
      output_stream::asm_stream::instance() << d;

      calc::stacks stacks;
      calc::operators operators;
      ops[1] = utils::smooth_op_exp(ops[1]);
      pegtl::parse_string<calc::calc_grammar, calc::action>(ops[1], ops[1],
                                                            operators, stacks);

      auto r = stacks.finish();
      utils::number_warn(in, ops[1], r, 1, true);
      output_stream::asm_stream::instance() << utils::mask_int_to_nibble(r);
    }
  };

  template <>
  struct action<b_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::b_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      char* end;
      std::string s = in.string();
      s = output_stream::asm_stream::replace_all(s, " ", "");

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::ext_;
      output_stream::asm_stream::instance() << utils::opcodes::b_;

      char d;
      d = (char)std::strtol(ops[0].c_str() + 2, &end, 10);
      output_stream::asm_stream::instance() << d;
    }
  };

  template <>
  struct action<bz_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::bz_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      char* end;
      std::string s = in.string();
      s = output_stream::asm_stream::replace_all(s, " ", "");

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::ext_;
      output_stream::asm_stream::instance() << utils::opcodes::bz_;

      char d;
      d = (char)std::strtol(ops[0].c_str() + 3, &end, 10);
      output_stream::asm_stream::instance() << d;
    }
  };

  template <>
  struct action<bnz_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::bnz_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      char* end;
      std::string s = in.string();
      s = output_stream::asm_stream::replace_all(s, " ", "");

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::ext_;
      output_stream::asm_stream::instance() << utils::opcodes::bnz_;

      char d;
      d = (char)std::strtol(ops[0].c_str() + 4, &end, 10);
      output_stream::asm_stream::instance() << d;
    }
  };

  template <>
  struct action<bs_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::bs_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      char* end;
      std::string s = in.string();
      s = output_stream::asm_stream::replace_all(s, " ", "");

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::ext_;
      output_stream::asm_stream::instance() << utils::opcodes::bs_;

      char d;
      d = (char)std::strtol(ops[0].c_str() + 3, &end, 10);
      output_stream::asm_stream::instance() << d;
    }
  };

  template <>
  struct action<stat_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::stat_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      char* end;
      std::string s = in.string();
      s = output_stream::asm_stream::replace_all(s, " ", "");
      s = output_stream::asm_stream::replace_all(s, "\n", "");
      s = output_stream::asm_stream::replace_all(s, "\t", "");

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::ext_;
      output_stream::asm_stream::instance() << utils::opcodes::stat_;

      char d;
      d = (char)std::strtol(ops[0].c_str() + 5, &end, 10);
      output_stream::asm_stream::instance() << d;

      calc::stacks stacks;
      calc::operators operators;
      ops[1] = utils::smooth_op_exp(ops[1]);
      pegtl::parse_string<calc::calc_grammar, calc::action>(ops[1], ops[1],
                                                            operators, stacks);

      auto r = stacks.finish();
      utils::number_warn(in, ops[1], r, 1, false);
      output_stream::asm_stream::instance() << utils::mask_int_to_nibble(r);
    }
  };

  template <>
  struct action<check_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::check_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      std::string s = in.string();
      s = output_stream::asm_stream::replace_all(s, " ", "");

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::ext_;
      output_stream::asm_stream::instance() << utils::opcodes::check_;
    }
  };

  template <>
  struct action<mode_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::mode_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      std::string s = in.string();
      s = output_stream::asm_stream::replace_all(s, " ", "");

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::ext_;
      output_stream::asm_stream::instance() << utils::opcodes::mode_;

      std::string mode = std::string(ops[0].c_str() + 4);
      std::transform(mode.begin(), mode.end(), mode.begin(), ::tolower);
      output_stream::asm_stream::instance()
          << utils::get_mode(std::string(ops[0].c_str() + 4));
    }
  };

  template <>
  struct action<fork_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::fork_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      std::string s = in.string();
      s = output_stream::asm_stream::replace_all(s, " ", "");

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::ext_;
      output_stream::asm_stream::instance() << utils::opcodes::fork_;
    }
  };

  template <>
  struct action<write_inst>
  {
    static void apply(const pegtl::action_input& in)
    {
      states::parser_state::instance().incr_curr_mem_addr(
          utils::inst_size_imm(utils::inst_size::write_));

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      char* end;
      std::string s = in.string();
      s = output_stream::asm_stream::replace_all(s, " ", "");
      s = output_stream::asm_stream::replace_all(s, "_", "");
      s = output_stream::asm_stream::replace_all(s, "\n", "");
      s = output_stream::asm_stream::replace_all(s, "\t", "");

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ',');

      output_stream::asm_stream::instance() << utils::opcodes::ext_;
      output_stream::asm_stream::instance() << utils::opcodes::write_;

      char d = (char)std::strtol(ops[0].c_str() + 6, &end, 10);
      output_stream::asm_stream::instance() << d;
    }
  };

  template <>
  struct action<align>
  {
    static void apply(const pegtl::action_input& in)
    {
      std::string s = in.string();

      calc::stacks stacks;
      calc::operators operators;

      std::vector<std::string> ops = output_stream::asm_stream::split(s, ' ');

      pegtl::parse_string<calc::calc_grammar, calc::action>(ops[1], ops[1],
                                                            operators, stacks);

      auto r = stacks.finish();
      auto at = states::parser_state::instance().get_current_mem_addr();

      unsigned long div = utils::next_divisor(in, at, r);

      states::parser_state::instance().incr_curr_mem_addr(div - at);

      if (states::parser_state::instance().get_state() == states::pass::pass1)
        return;

      for (unsigned long i = 0; i < div - at; i++)
        output_stream::asm_stream::instance() << 1;
    }
  };

  template <>
  struct action<ship_name>
  {
    static void apply(const pegtl::action_input& in)
    {
      if (states::parser_state::instance().get_state() == states::pass::pass2)
        return;

      std::string s = in.string();
      unsigned int i;
      for (i = 0; i < s.size(); i++)
        if (s[i] == '\"')
          break;
      s = output_stream::asm_stream::replace_all(s, "\"", "");

      if (s.size() > NAME_LENGTH)
        utils::warn(in, "ship's name is too long");

      output_stream::asm_stream::instance().set_ship_name(
          s.substr(i, s.size() - i));
    }
  };

  template <>
  struct action<ship_comment>
  {
    static void apply(const pegtl::action_input& in)
    {
      if (states::parser_state::instance().get_state() == states::pass::pass2)
        return;

      std::string s = in.string();
      unsigned int i;
      for (i = 0; i < s.size(); i++)
        if (s[i] == '\"')
          break;
      s = output_stream::asm_stream::replace_all(s, "\"", "");

      if (s.size() > COMMENT_LENGTH)
        utils::warn(in, "ship's comment is too long");

      output_stream::asm_stream::instance().set_ship_comment(
          s.substr(i, s.size() - i));
    }
  };

  template <>
  struct action<label>
  {
    static void apply(const pegtl::action_input& in)
    {
      if (states::parser_state::instance().get_state() == states::pass::pass2)
        return;

      states::parser_state::instance().store_label(
          in, in.string().substr(0, in.string().size() - 1),
          states::parser_state::instance().get_current_mem_addr());
    }
  };
}
