#pragma once

#include "include/pegtl.hh"
#include <iostream>
#include <set>
#include <string>
#include <utility>

namespace states
{
  enum pass
  {
    pass1 = 0,
    pass2 = 1,
  };

  typedef std::pair<std::string, long int> label;

  class parser_state
  {
  public:
    static parser_state& instance()
    {
      static parser_state s;
      return s;
    }

    pass get_state();
    void set_state(pass curr_state);
    void store_label(const pegtl::action_input& in, std::string name,
                     long int addr);
    unsigned long int get_current_mem_addr();
    void incr_curr_mem_addr(unsigned long int inc);
    void reset_curr_mem_addr();
    unsigned long get_label_addr(std::string name);
    bool label_exists(std::string name);
    int get_errors();
    void incr_err();

  private:
    unsigned long int pc_ = 0UL;
    int errors_ = 0;

    pass s_ = pass1;
    std::set<std::string> str_labels_;
    std::set<label> labels_;
  };
}
