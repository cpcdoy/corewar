#pragma once

#include "instructions.hh"
#include "state.hh"
#include "utils.hh"
#include <cassert>
#include <functional>
#include <iostream>
#include <map>
#include <vector>

constexpr int MAX_INT = 2147483647;

namespace calc
{
  enum class order : int
  {
  };

  struct op
  {
    order p;
    std::function<long(long, long)> f;
  };

  struct stack
  {
    void push(const op& b)
    {
      while ((!m_o.empty()) && (m_o.back().p <= b.p))
        reduce();
      m_o.push_back(b);
    }

    void push(const long l)
    {
      m_l.push_back(l);
    }

    long finish()
    {
      while (!m_o.empty())
        reduce();
      assert(m_l.size() == 1);
      const auto r = m_l.back();
      m_l.clear();
      return r;
    }

  private:
    std::vector<op> m_o;
    std::vector<long> m_l;

    void reduce()
    {
      assert(m_o.size() > 0);
      assert(m_l.size() > 1);

      const auto r = m_l.back();
      m_l.pop_back();
      const auto l = m_l.back();
      m_l.pop_back();
      const auto o = m_o.back();
      m_o.pop_back();
      m_l.push_back(o.f(l, r));
    }
  };

  struct stacks
  {
    stacks()
    {
      open();
    }

    void open()
    {
      m_v.push_back(stack());
    }

    template <typename T>
    void push(const T& t)
    {
      assert(!m_v.empty());
      m_v.back().push(t);
    }

    void close()
    {
      assert(m_v.size() > 1);
      const auto r = m_v.back().finish();
      m_v.pop_back();
      m_v.back().push(r);
    }

    long finish()
    {
      assert(m_v.size() == 1);
      return m_v.back().finish();
    }

  private:
    std::vector<stack> m_v;
  };

  struct operators
  {
    operators()
    {
      insert("*", order(5), [](const long l, const long r) { return l * r; });
      insert("/", order(5), [](const long l, const long r) { return l / r; });
      insert("%", order(5), [](const long l, const long r) { return l % r; });
      insert("+", order(6), [](const long l, const long r) { return l + r; });
      insert("-", order(6), [](const long l, const long r) { return l - r; });
    }

    void insert(const std::string& name, const order p,
                const std::function<long(long, long)>& f)
    {
      assert(!name.empty());
      m_ops.insert({name, {p, f}});
    }

    const std::map<std::string, op>& ops() const
    {
      return m_ops;
    }

  private:
    std::map<std::string, op> m_ops;
  };

  using namespace pegtl;

  struct comment : if_must<one<'#'>, until<eolf>>
  {
  };

  struct ignored : sor<plus<space>, comment>
  {
  };

  struct infix
  {
    using analyze_t = analysis::generic<analysis::rule_type::ANY>;

    template <apply_mode A, template <typename...> class Action,
              template <typename...> class Control, typename Input>
    static bool match(Input& in, const operators& b, stacks& s)
    {
      return match(in, b, s, std::string());
    }

  private:
    template <typename Input>
    static bool match(Input& in, const operators& b, stacks& s, std::string t)
    {
      if (in.size(t.size() + 1) > t.size())
      {
        t += in.peek_char(t.size());
        const auto i = b.ops().lower_bound(t);
        if (i != b.ops().end())
        {
          if (match(in, b, s, t))
          {
            return true;
          }
          else if (i->first == t)
          {
            s.push(i->second);
            in.bump(t.size());
            return true;
          }
        }
      }
      return false;
    }
  };

  struct name : plus<sor<one<'_'>, pegtl::alnum>>
  {
  };

  struct number
      : sor<name,
            seq<opt<one<'%', '+', '-', '0'>>,
                seq<opt<one<'%'>>, opt<one<'0'>>, opt<one<'x'>>>, plus<xdigit>>>
  {
  };

  struct expression;

  struct bracket : if_must<one<'('>, expression, one<')'>>
  {
  };

  struct atomic : sor<number, bracket>
  {
  };

  struct expression : list<atomic, infix, ignored>
  {
  };

  struct calc_grammar : must<expression, eof>
  {
  };

  template <typename Rule>
  struct action : pegtl::nothing<Rule>
  {
  };

  template <typename Input>
  long binary_res(const Input& in, char* end)
  {
    int sign = in.string()[0] == '-' ? 1 : 0;
    auto r = (sign ? -1 : 1) *
             std::strtol(in.string().c_str() + (sign ? 2 : 1), &end, 2);
    if (r > MAX_INT)
      utils::error(in, in.string(), std::string("Number is too big").c_str());
    return r;
  }

  template <typename Input>
  long hex_res(const Input& in, char* end)
  {
    auto r = std::strtol(in.string().c_str(), &end, 16);
    if (r > MAX_INT)
      utils::error(in, in.string(), std::string("Number is too big").c_str());
    return r;
  }

  template <typename Input>
  long oct_res(const Input& in, char* end)
  {
    auto r = std::strtol(in.string().c_str(), &end, 8);
    if (r > MAX_INT)
      utils::error(in, in.string(), std::string("Number is too big").c_str());
    return r;
  }

  template <typename Input>
  long label_res(const Input& in)
  {
    auto r = states::parser_state::instance().get_label_addr(in.string());
    if (r == (unsigned long)-1)
      std::cerr << "Error: label \"" << in.string() << "\" does not exist"
                << std::endl;
    return r;
  }

  template <>
  struct action<number>
  {
    template <typename Input>
    static void apply(const Input& in, const operators&, stacks& s)
    {
      char* end = nullptr;
      if ((in.string()[0] == '-' && in.string()[1] == '%') ||
          in.string()[0] == '%')
        s.push(binary_res(in, end));
      else if ((in.string()[0] == '-' && in.string()[1] == '0' &&
                in.string()[2] == 'x') ||
               (in.string()[0] == '0' && in.string()[1] == 'x'))
        s.push(hex_res(in, end));
      else if ((in.string()[0] == '-' && in.string()[1] == '0') ||
               in.string()[0] == '0')
        s.push(oct_res(in, end));
      else if (utils::is_alnum(in.string()) &&
               !std::isdigit(in.string().c_str()[0]))
        s.push(label_res(in));
      else
      {
        auto r = 1;
        try
        {
          r = std::stol(in.string());
        }
        catch (std::out_of_range)
        {
          utils::error(in, in.string(),
                       std::string("Number is too big").c_str());
        }
        s.push(r);
      }
    }
  };

  template <>
  struct action<seq<number, one<'*', '/'>>>
  {
    template <typename Input>
    static void apply(const Input&, const operators&, stacks& s)
    {
      s.open();
    }
  };

  template <>
  struct action<seq<one<'*', '/'>, number>>
  {
    template <typename Input>
    static void apply(const Input&, const operators&, stacks& s)
    {
      s.close();
    }
  };

  template <>
  struct action<one<'('>>
  {
    template <typename Input>
    static void apply(const Input&, const operators&, stacks& s)
    {
      s.open();
    }
  };

  template <>
  struct action<one<')'>>
  {
    template <typename Input>
    static void apply(const Input&, const operators&, stacks& s)
    {
      s.close();
    }
  };
}
