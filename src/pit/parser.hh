#pragma once

#include <cstdio>
#include <cstring>
#include <fcntl.h>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>
#include <unordered_map>

#include "action.hh"
#include "tracer.hh"

namespace parser
{
  typedef int (*handler)(void);

  void parse(char* file_path);
  int parse_args(int argc, char** argv);
}
